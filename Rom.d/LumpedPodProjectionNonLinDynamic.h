#ifndef ROM_LUMPEDPODPROJECTIONNONLINDYNAMIC_H
#define ROM_LUMPEDPODPROJECTIONNONLINDYNAMIC_H

#include "PodProjectionNonLinDynamic.h"

#include <map>

namespace Rom {

class LumpedPodProjectionNonLinDynamic : public PodProjectionNonLinDynamic {
public:
  explicit LumpedPodProjectionNonLinDynamic(Domain *);

  virtual void preProcess();
  virtual void updateStates(ModalGeomState *refState, ModalGeomState& geomState, double time);
  virtual void setLocalReducedMesh(int j);
  void getResidualSensitivity(ModalGeomState& geomState, Vector& residual, Vector& elementInternalForce,
                                                              double t, ModalGeomState *refState, Vector& external_force);  
  virtual void correctResidualSensitivity(ModalGeomState& geomState, Vector& residual, Vector& elementInternalForce,
                            double t, ModalGeomState *refState, Vector& external_force); 
  void getInitialResidualSensitivity(ModalGeomState& geomState, Vector& residual, Vector& elementInternalForce,
                                                              double t, ModalGeomState *refState, Vector& external_force, Eigen::MatrixXd& reducedMass); 

  
  void createKelArray(AllOps<double> &allOps, GenFullSquareMatrix<double> *&kelArray, GenFullSquareMatrix<double> *&melArray, GenFullSquareMatrix<double> *&celArray); 

private:
  virtual void getStiffAndForceFromDomain(GeomState &geomState, Vector &elementInternalForce,
                                          Corotator **allCorot, FullSquareMatrix *kelArray,
                                          Vector &residual, double lambda, double time, GeomState *refState,
                                          FullSquareMatrix *melArray, bool forceOnly);

protected:
  std::vector<std::map<int, double> > packedElementWeights_;
  std::vector<int> packedWeightedNodes_;
  std::set<int> packedWeightedElems_;
  void buildPackedElementWeights();
  void initializeDerivativeOfFictitiousForce(GeomState &geomState, DerivativeOfGeomState &dGeomState);

  template<class Scalar>
  void getStiffAndForceSensitivity(ModalGeomState &geomState, Vector &elementInternalForce,
                                                             Corotator **allCorot, FullSquareMatrix *kelArray,
                                                             Vector &residual_ , Vector& external_force, 
                                                             Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& derivativeTensor); 
  template<class Scalar>
  void getAdjointICcorrection(ModalGeomState &geomState, Vector &elementInternalForce,
                                                             Corotator **allCorot, FullSquareMatrix *kelArray,
                                                             Vector &residual_ , Vector& external_force, 
                                                             Eigen::MatrixXd& reducedMass, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& derivativeTensor); 

  int localReducedMeshId_;
  std::vector<std::vector<int> > localPackedWeightedNodes_;
  Eigen::VectorXd int_force; 
  Eigen::VectorXd ext_force; 
};

} /* end namespace Rom */

#endif /* ROM_LUMPEDPODPROJECTIONNONLINDYNAMIC_H */
