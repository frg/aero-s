#ifndef _CONTROL_INTERFACE_H_
#define _CONTROL_INTERFACE_H_

#include "Math.d/Vector.h"

#include <iostream>

template <class Vector> class SysState;
class SingleDomainDynamic;

class ControlInterface {

  protected:
    double dt;	       // time step size
    
  public:
  
    // set time step
    void setDt(double h) { dt = h; }

    // Control force initialization routine
    virtual void init(double *displacement, double *velocity, double *acc,
                      SingleDomainDynamic * probDesc=0) = 0;

    // Control force routine
    virtual void ctrl(double *dis, double *vel, double *acc, double *f,
                      double time=0, 
		      SysState<Vector> *state=0, Vector *ext_f=0) = 0;

    // User defined force routine
    virtual void usd_forc(double time, double *userDefineForc) = 0;

    // User defined displacement routine
    virtual void usd_disp(double time, double *userDefineDisp,
                          double *userDefineVel, double *userDefineAcc) = 0;
    
    // User defined joint routine for relative rotations (currently only for element 126,
    // but interface could be expanded to allow use for elements 226, 134 and 234) 
    //   
    // The mid is used for the use case that multiple joints are being driven with the
    // same control file (for example, if the relative rotation is being determined
    // simultanesouly for all joints, this allows that computation to be done once)
    //
    // Note that use of this feature requires a deleter function specified with
    // extern "C" void deleteControlObject()
    // This function deletes the control object to avoid memory leaks; if multiple
    // user-defined joint drivers use the same control object via the mid parameter,
    // it is up to the user to ensure that the above deleteControlObject function
    // checks to see if the pointer is null before deleting, and setting it to null
    // after deleting.
    virtual void usd_joint(double time, int mid, double *userDefineFunc,
                           double *userDefineFuncDeriv, double *userDefineFunc2ndDeriv) = 0;

    // Cost function for unsteady adjoint
    virtual double cost(double time, double *dis, double *vel, double *acc) {
      std::cerr << " *** WARNING: ControlInterface::usd_cost is not implemented\n";
      return 0;
    }

    // Partial derivative of cost function w.r.t. displacement for unsteady adjoint
    virtual void dcost_dd(double time, double *dis, double *vel, double *acc,
                          double *dC_dd) {
      std::cerr << " *** WARNING: ControlInterface::usd_dcost_dd is not implemented\n";
    }

    // Partial derivative of cost function w.r.t. velocity for unsteady adjoint
    virtual void dcost_dv(double time, double *dis, double *vel, double *acc,
                          double *dC_dv) {
      std::cerr << " *** WARNING: ControlInterface::usd_dcost_dv is not implemented\n";
    }

    // Product of transpose of Jacobian of usd_forc w.r.t beta and vector for unsteady adjoint
    // i.e. [∂F/∂β]^T*y or equivalently y^T*[∂F/∂β]
    virtual void usd_forc_apply_jac_tr(double time, double *y, double *result) {
      std::cerr << " *** WARNING: ControlInterface::usd_forc_apply_jac_tr is not implemented\n";
    }

    // Product of transpose of Jacobian of ctrl w.r.t beta and vector for unsteady adjoint
    // i.e. [∂F/∂β]^T*y or equivalently y^T*[∂F/∂β]
    virtual void ctrl_apply_jac_tr(double *dis, double *vel, double *acc, double time, double *y, double *result) {
      std::cerr << " *** WARNING: ControlInterface::ctrl_apply_jac_tr is not implemented\n";
    }

};

// type of destroy function (currently only used for user defined joint drivers)
typedef void destroy_t();

#endif
