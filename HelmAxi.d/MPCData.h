#ifndef _MPCDATA_H_
#define _MPCDATA_H_


#include <Utils.d/resize_array.h>


struct AxiMPC {
  int nnum; 
  double angle1;
  double angle2;
  int slices;
};


inline AxiMPC
MPC(int node, double angle1, double angle2, int slices) {

  AxiMPC r;

  r.nnum = node;
  r.angle1 = angle1;
  r.angle2 = angle2;
  r.slices = slices;

  return r;

}


class MPCData {

public : 

  int numMPCSet;
  int totalNumMPC;
  ResizeArray<AxiMPC> term;   

  int numMPCPoint;
  ResizeArray<int> nodeNb;

  // Complex value of type c*e^(i*k*(x*dx+y*dy+z*dz))
  DComplex cstant;
  double dx,dy,dz;

  MPCData();
  void addMPCSet(AxiMPC &mpc);
  void addMPCPoint(int nn);
  void setConst(DComplex);
  void setDir(double _dx, double _dy, double _dz);
 
};


extern MPCData *globalMPCs;


#endif
