#ifndef  _AXIHELEM_H_
#define  _AXIHELEM_H_

#include <Element.d/Element.h>
#include <cstdio>

class AxiHElement: public Element {
  public:
    virtual FullSquareMatrix stiffteta(const CoordSet&, double *d) const = 0;
    virtual void buildMesh3D(int &eNum, FILE *, int nodeInc, int nSlices) = 0;
    virtual void buildMesh2D(int &eNum, FILE *, int nodeInc, int nSlices) = 0;
};
#endif
