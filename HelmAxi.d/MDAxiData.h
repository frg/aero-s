#ifndef _MDAXIDATA_H_
#define _MDAXIDATA_H_
#include <Math.d/SymFullMatrix.h>

template <class Scalar> class GenSubDomain;
typedef GenSubDomain<double> SubDomain;
class FourierHelmBCs;
template<class Scalar> class GenVector;
typedef GenVector<DComplex> ComplexVector;
template<class Scalar> class GenCuCSparse;
typedef GenCuCSparse<DComplex> CuCComplexSparse;
template<class Scalar> class GenDBSparseMatrix;
typedef GenDBSparseMatrix<DComplex> DBComplexSparseMatrix;
template<class Scalar> class GenSolver;
typedef GenSolver<DComplex> ComplexSolver;
class Polygon;
class LineAxiSommer;
template <class Scalar> class GenSparseSet;
typedef GenSparseSet<DComplex> ComplexSparseSet;
template <class Scalar> class GenSymFullMatrix;
typedef GenSymFullMatrix<DComplex> SymFullMatrixC;
template <class Scalar> class GenFullSquareMatrix;
typedef GenFullSquareMatrix<DComplex> GenFullSquareMatrixC;
class DofSetArray;
class Connectivity;
class MPCData;
template <class Scalar> class GenFullM;
typedef GenFullM<DComplex> FullMC;
template <class Scalar> class GenSparseMatrix;
typedef GenSparseMatrix<double> SparseMatrix;
typedef GenSparseMatrix<DComplex> ComplexSparseMatrix;
class MDAxiData;
template <class Scalar> class FSCommPattern;

struct GenHelmParam {
   double kappaHelm;
};

// Legacy Subdomain Communication class
class MDAxiSComm {
 public:
   int locSubNum; // when running in distributed mode
   int *glSubToLocal; // mapping for distributed mode

   int numNeighb;   // Number of neighbors
   int *subNums;    // neighbor identification numbers
   int *remoteId;   // id of this subdomain in the corresponding neighbor
   Connectivity *sharedNodes; // nodes shared with the neighbors
   Connectivity *sharedDOFs;  // DOFs # shared

   MDAxiSComm(int, int *, int*, Connectivity *);
   gl_sub_idx neighborIndex(int i);
};

class MDAxiData : public GenSubDomain<DComplex> {

    using FetiBaseSub::weight;
    MDAxiSComm *scomm;

    GenHelmParam *hParams;

    ComplexSolver **KC;
    CuCComplexSparse **KucC;
    DBComplexSparseMatrix **KbbC;   // For preconditioning
    CuCComplexSparse **KibC;
    ComplexSolver **KiiC;
    DBComplexSparseMatrix **KccC;   // To get normal derivative on body

    int numInterface;
    SommerElement **InterfElem;
    int* InterfaceIndex;

    int numInQSet;
    ComplexSparseSet **QSet;
    DComplex **BKQ;

    ComplexVector **localCVec;

    public :
    long long memKC;
    long long memKibC;
    long long memKucC;
    long long memKccC;

    long long memC;
    long long memQ;
    long long memCKQ;
    long long memCKCt;

    int subType;

    MPCData *locMPCs;
    DComplex **CKQ;
    SymFullMatrixC *CKCt;
    int *localToGlobalMPC;
    int *globalToLocalMPC;

    int halfOffset;

    FourierHelmBCs *locBCs;
    int *locToglDir;

    int InterfSign;
    DComplex *interfBuff;

    int *coarseEqNums;
    int numCGridDofs;

    MDAxiData(Domain *d, int n, Connectivity *cn, Connectivity *sToN,
              FourierHelmBCs *glBCs, int gn, int type);

    void setMDAxiSComm(MDAxiSComm *sc) { scomm = sc; }
    MDAxiSComm *getMDAxiSComm() { return scomm; }

    void extractBCs(FourierHelmBCs *glBCs, Connectivity *nodeToSub);
    void extractMPCs(MPCData *glMPCs, Connectivity *mpcToSub,
                     Connectivity *nodeToSub);

    int* getInterfaceIndex();

    void renumberData();

    void getInterface(PolygonSet ***allPs, FSCommPattern<int> *psPat); 
    void sendData(PolygonSet ***allPs, FSCommPattern<int> *psPat);
    void finishInterface(PolygonSet ***allPs, FSCommPattern<int> *psPat);

    void sendDOFList(FSCommPattern<int> *pat);
    void gatherDOFList(DofSet ***allBoundary, FSCommPattern<int> *pat);

    void defineKs(int **, int **);

    void makeKs(int *, int *, int m1=0, int m2=-1);
    void Assemble(int m1=0, int m2=-1);
    void AssembleFict();
    void addInterface(int *, FSCommPattern<int> *, int m1=0, int m2=-1);

    void deleteGlMap(int **, int **);

    void factorKC(int m1=0, int m2=-1);
    void factorKiiC(int m1=0, int m2=-1);

    void prepareCoarseData(DofSet ***, int *, int **, int **, int **,
                           DComplex ***);
    void makeCoarseData(int *, int **, int **, int **, DComplex ***, int f1=0,
                        int f2=-1);
    void deleteCoarseInfo(int **, int **, int **, DComplex ***);
    DComplex CoarseGridValue(int, double, double, DComplex);
    void setNumCoarseDofs();
    int numCoarseDofs();
    void makeCKCt();
    void prepareMPCSet(int f1=0, int f2=-1);

    void allocateCoarse(int);
    void assembleCoarse(int, ComplexSparseMatrix *, FullMC **, int, int);
    void computeQtKQ(int, int, int, DComplex **, DComplex *);
    void assembleQtKQ(int, int, int, DComplex *, ComplexSparseMatrix *);
    void assembleCKQSet(int, int, int, DComplex *, FullMC **);
    void deleteConn();
    void makeCoarseGridDofs(DofSetArray *, Connectivity *);

    void buildRHS(DComplex *, int f1=0, int f2=-1);

    void assembleDUDN(DComplex *u, DComplex **dudn, int f1=0, int f2=-1);
    void preProcessDN(int *, int *, int **);
    void preProcessDN2(int *, int *, int **);
    void localDUDN(DComplex **u, DComplex **dudn, int *, int *, int *, int **,
                   int f1=0, int f2=-1);
    void localDUDN2(DComplex **u, DComplex **dudn, int *, int *, int *, int **,
                   int f1=0, int f2=-1);

    DComplex *getLocalCVec(int);
    void zeroLocalCVec(int f1=0, int f2=-1);

    void multLocalQtBK(DComplex *, int f1=0, int f2=-1);
    void multKf(DComplex *, DComplex *, int shiftu=0, int shiftf=0, int f1=0,
                int f2=-1);
    void computeBw(DComplex *, DComplex *, int f1=0, int f2=-1);
    void setMDAxiDofCommSize(FSCommPattern<DComplex> *vPat) const;
    void setMDAxiCommSize(FSCommPattern<int> *sPat, int size) const;
    void sendInterf(DComplex *, FSCommPattern<DComplex> *);
    void interfaceJump(DComplex *, FSCommPattern<DComplex> *);
    void multKbbC(DComplex *, DComplex *, DComplex *, DComplex *, int f1=0,
                  int f2=-1);
    void sendDeltaF(DComplex *, FSCommPattern<DComplex> *);
    DComplex collectAndDotDeltaF(DComplex *, FSCommPattern<DComplex> *);
    void multTransposeBKQ(DComplex *, int f1=0, int f2=-1);
    void finishFPz(ComplexVector **, DComplex *, DComplex *, int f1=0,
                   int f2=-1);
    void getHalfInterf(DComplex *, DComplex *);
    void getHalfInterf(DComplex *, DComplex *, DComplex *, DComplex *);
    void scatterHalfInterf(DComplex *, DComplex *);
    void rebuildInterf(DComplex *, FSCommPattern<DComplex> *);
    void subtractBt(DComplex *, DComplex *, int f1=0, int f2=-1);
    void finishBtPz(ComplexVector **, DComplex *,DComplex *);

    double getWaveNumber() { return hParams->kappaHelm; }
    int getMPCSize();

    void mergeSolution(DComplex **, DComplex *);

    void subtractQw(DComplex *f, ComplexVector **w, ComplexVector *mu=0,
                       int f1=0, int f2=-1);
    void addCKQw(ComplexVector **, ComplexVector *, int f1=0, int f2=-1);
    void finishFzl(DComplex *, DComplex *, ComplexVector **, DComplex *,
                   int f1=0, int f2=-1);

    double squaredNormL2(DComplex **, int f1=0, int f2=-1);

};

#endif
