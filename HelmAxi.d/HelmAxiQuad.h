#ifndef _HELMAXIQUAD_H_
#define _HELMAXIQUAD_H_

#include <HelmAxi.d/AxiHElem.h>

class PolygonSet;

class HelmAxiQuad: public AxiHElement {

	int nn[4];
public:
	HelmAxiQuad(int*);
	int getElementType() const override { return 60; }
	Category getCategory() const override { return Category::Acoustic; }
	Element *clone() override;
	void renum(const int *) override;
        void renum(EleRenumMap&) override;
        FullSquareMatrix stiffness(const CoordSet&, double *d, int flg = 1) const override;
        FullSquareMatrix stiffteta(const CoordSet&, double *d) const override;
        FullSquareMatrix massMatrix(const CoordSet&, double *d, int cmflg = 1) const override;
	void    markDofs(DofSetArray &) const override;
        int*    dofs(DofSetArray &, int *p=0) const override;
        int     numDofs() const override;
        int     numNodes() const override;
        int*    nodes(int * = 0) const override;
	void	addFaces(PolygonSet *pset) override;
        void    buildMesh3D(int &elemNum, FILE *outF,
                            int nodeInc, int numSlices) override;
        void    buildMesh2D(int &elemNum, FILE *outF,
                            int nodeInc, int numSlices) override;
	PrioInfo examine(int sub, MultiFront *) override;
	int getTopNumber() const override { return 130; }
};
#endif

