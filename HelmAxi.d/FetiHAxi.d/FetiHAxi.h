#ifndef _FETIHAXISOLVER_H_
#define _FETIHAXISOLVER_H_

#include <Math.d/ComplexD.h>
#include <Math.d/Skyline.d/SkyMatrix.h>
#include <HelmAxi.d/FetiHAxi.d/AugmentedGMRESC.h>
#include <memory>

class MDAxiData;
class FetiInfo;
class Connectivity;
class DofSetArray;
class compStruct;
class DistrInfo;
class DistrComplexVector;
template<class Scalar> class GenVector;
typedef GenVector<DComplex> ComplexVector;
template<class Scalar> class GenSolver;
typedef GenSolver<DComplex> ComplexSolver;
class GCRC;
template <class Scalar> class GenFullM;
typedef GenFullM<DComplex> FullMC;
class Timings;
template<class Scalar> class FSCommPattern;

class FetiHAxiSolver {

    MDAxiData **mdAxi;
    int nsub;
    int nsubReal;
    int nsubFict;
    int numModes;

    FetiInfo *fetiInfo;

    Connectivity *subToSub;
    Connectivity *edgeToSub, *subToEdge, *edgeToEdge;
    DofSetArray *coarseEqs;
    compStruct renumber;

    int halfSize;
    DistrInfo internal, interface;

    double epsilon2;
    int maxiter;

    ComplexSolver **CoarseSolver;
    ComplexVector **CoarseVector;

    GCRC *oSet;
    mutable std::unique_ptr<AugmentedGMRESC> agmres = nullptr;

    DistrComplexVector *deltaU;
    DistrComplexVector *deltaF;

    int MPCSize;
    FullMC **CoarseMPC;
    SkyMatrixC *MPCSolver;

    Timings times;

    int totBlock;
    int *stepRHS;

    FSCommPattern<DComplex> *vPat = nullptr;
    Connectivity *cpuToSub;
    FSCommunicator *fetiCom;

  public :

    FetiHAxiSolver(int, int, MDAxiData **, Connectivity *, int, FetiInfo *, Connectivity *, FSCommunicator *);
    void computeLengths(int &, DistrInfo &, DistrInfo &);

    void defineMatrices(int, int **, int **);
    void constructAndAssembleMat(int, int *, int **, int **, FSCommPattern<int> *);
    void deleteGlMap(int, int **, int **);
    void factorMatrices(int iP);

    void allocateBuffer(int nSub);
    void countEdges(int iSub, int *edges);
    void numberEdges(int iSub, int *eC, int *eP, int *edges, FSCommPattern<int> *pat);
    void receiveNeighbEdgeNums(int iSub, int *eP, int* edges, FSCommPattern<int> *pat);
    void makeEdgeConnectivity();
    void makeCoarse();
    void allocateCoarseSolver(int, Connectivity *);
    void allocateCoarseVectors(int);
    void makeCoarseGridDofs(int);
    void assembleCoarse(int);
    void factorCoarseMatrices(int);
    void makeStaticLoad(DistrComplexVector &f);
    void makeLocalStaticLoad(int iSub, DistrComplexVector &f);
    DistrInfo &localInfo()  { return internal; }


    void solve(DistrComplexVector &, DistrComplexVector &);


    void computePtd(DistrComplexVector &, DistrComplexVector &);
    void zeroLocalCVec(int);
    void multQtBK(int, DistrComplexVector &);
    void assembleCoarseVector(int);
    void solveCoarsePb(int);
    void subtractQw(int, DistrComplexVector &);
    void multBK(int, DistrComplexVector &, DistrComplexVector &);


    void step1(int, DistrComplexVector &);
    void step3(int, DistrComplexVector &);
    void sendInterf(int, DistrComplexVector &, int &);
    void InterfDiff(int, DistrComplexVector &, int &);


    double preCondition(DistrComplexVector &, DistrComplexVector &);
    void multKbb(int, DistrComplexVector &, DistrComplexVector &,
                 DistrComplexVector &, DistrComplexVector &);
    void sendDeltaF(int, DistrComplexVector *, int);
    void normDeltaF(int, DComplex *, DistrComplexVector *, int);
    void precon1(int, DistrComplexVector *);
    void precon3(int, DComplex *, DistrComplexVector *);


    void applyFP(DistrComplexVector &, DistrComplexVector &);
    void multTransposeBKQ(int, DistrComplexVector &);
    void finishFPz(int, DistrComplexVector &, DistrComplexVector &);


    int predict(DistrComplexVector &, DistrComplexVector &);
    void orthogonalize(DistrComplexVector &, DistrComplexVector &,
                       DistrComplexVector &, DistrComplexVector &);
    void gatherHalfInterface(int, DistrComplexVector *,
                      DistrComplexVector *, DComplex *, DComplex *);
    void GHI(int, DistrComplexVector *,
                      DistrComplexVector *, DComplex *, DComplex *);
    void scatterHalfInterface(int, DComplex *, DistrComplexVector *,
                              int *);
    void inter1(int, DComplex *, DistrComplexVector*);
    void rebuildInterface(int, DistrComplexVector &, int &);
    void inter3(int, DistrComplexVector &);
    void orthoAdd(DistrComplexVector &, DistrComplexVector &, DComplex);


    void recoverU(DistrComplexVector &, DistrComplexVector &,
                  DistrComplexVector &);
    void finishBtPz(int, DistrComplexVector &,DistrComplexVector &);
    void subtractBt(int, DistrComplexVector &, DistrComplexVector &);
    void localSolution(int, DistrComplexVector &,DistrComplexVector &);


    Timings& getTimers();
    double getSolutionTime();

    void solveMPC(DistrComplexVector &, ComplexVector &,
                  DistrComplexVector &);
    void solveMPC_GCR(DistrComplexVector &, ComplexVector &,
                      DistrComplexVector &);
    void solveMPC_AGMRES(DistrComplexVector &, ComplexVector &,
                         DistrComplexVector &);
    void buildMPCSolver();
    void startSchur(int Fourier);
    void finishSchur(int Fourier);
    void computePtd(DistrComplexVector &rl, DistrComplexVector &f,
                    ComplexVector &g, ComplexVector &muVector,
                    ComplexVector **lambdaVec, double &);
    void assembleMPCVector(int, DComplex *, double *);
    void copyMu(int, DComplex *, DComplex *);
    void buildRHSSchur(int, DComplex *);
    void recoverSchur(int, ComplexVector **, ComplexVector *);
    void subtractQwMPC(int iSub, DistrComplexVector *f,
                    ComplexVector **lambdaVec, ComplexVector *mu);
    void applyFPMPC(DistrComplexVector &, DistrComplexVector &,
                    ComplexVector **lambdaVec, double &);
    void addCKQ(int, ComplexVector **, ComplexVector *);
    void addCKC(int, double *, ComplexVector *, DComplex *);
    void finishFzl(int, DistrComplexVector &, ComplexVector &,
                   ComplexVector **&, DistrComplexVector &);
    void recoverUMPC(DistrComplexVector &, DistrComplexVector &,
                  ComplexVector &, DistrComplexVector &,
                  ComplexVector &muVector, ComplexVector **lambdaVec);

    void defineBlockLock(int);
};

#endif
