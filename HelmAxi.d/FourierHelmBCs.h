#ifndef _FOURIER_HELMBCS_H_
#define _FOURIER_HELMBCS_H_

#include <Utils.d/resize_array.h>

#include <array>
#include <vector>

class SommerElement;
template<class Scalar> class GenVector;
typedef GenVector<DComplex> ComplexVector;

struct ComplexFDBC {
// Complex Fourier BC of type c*e^(i*k*(x*dx+y*dy+z*dz))
  int nnum;
};

inline ComplexFDBC
FDBC(int n)
 { ComplexFDBC r; 
   r.nnum = n; 
   return r;
 }

struct ComplexFNBC {
// Complex Fourier BC of type c*e^(i*k*(x*dx+y*dy+z*dz))
  int nnum1, nnum2, nnum3;
};

inline ComplexFNBC
FNBC(int n1, int n2)
 { ComplexFNBC r;
    r.nnum1 = n1; r.nnum2 = n2; r.nnum3 = -1;
   return r;
 }

inline ComplexFNBC
FNBC(int n1, int n2, int n3)
 { ComplexFNBC r;
    r.nnum1 = n1; r.nnum2 = n2; r.nnum3 = n3;
   return r;
 }

struct fictitiousSurf {

 // Structure for describing a fictitious surface inside the mesh
 // type = -1 : no rotation - direct integration on the element
 // type = 0  : ele is rotated between boundL <= y <= boundR around Oz (x>0)
 // slices   : number of elements in [boundL, boundR]

 int type;
 int slices;
 double boundL, boundR;
 SommerElement *ele;
 int etype;

};

inline fictitiousSurf part(int t, int _slice, double bl, double br,
                           SommerElement *_el, int _etype) {

 fictitiousSurf s;
 s.type = t;
 s.slices = _slice;
 s.boundL = bl;
 s.boundR = br;
 s.ele = _el;
 s.etype = _etype;
 return s;

};

class FourierHelmBCs {

   public : 

   double kappa;
   double kImag;

   DComplex cstant;
   double dirX, dirY, dirZ;
   std::vector<std::array<double, 3>> directions;

   int numDir;
   ResizeArray<ComplexFDBC> dirBC;

   int numNeu;
   ResizeArray<ComplexFNBC> neuBC;

   int numSomm;
   int somType;
   double surfR, surfZ;
   ResizeArray<SommerElement *> somBC;

   int numPart;
   ResizeArray<fictitiousSurf> fictSurf;

   int numModes;
   int numSlices;
   int numWaves; // number of incident plane waves

   FourierHelmBCs();
   void addDirichlet(ComplexFDBC &boundary);
   void addNeuman(ComplexFNBC &boundary);
   void addSommer(SommerElement *ele);
   void setWaveNumber(double, double);
   void setModes(int mode);
   void setSlices(int slice);
   void setWaves(int wave);
   void setSurf(double aR, double aZ);
   void setSomType(int type);
   void setDir(double dx, double dy, double dz);
   void addDir(double dx, double dy, double dz);
   void initDir(int iDir);
   void setConst(DComplex C);
   DComplex Dir2Four(int mode, double x, double y);
   DComplex Neu2Four(int mode, double x, double y, double nr, double nz);
   void IntNeu2(int mode, double *x, double *y, double nx, double ny, DComplex *T);
   void IntNeu3(int mode, double *x, double *y, double nx, double ny, DComplex *T);

   void addFictPart(int, int, int, double, double, int *);
   void addPartFict(fictitiousSurf &s);
   void rhsFictPart(int, int, CoordSet &, ComplexVector &);
  
};

extern FourierHelmBCs *fourHelmBC;

#endif
