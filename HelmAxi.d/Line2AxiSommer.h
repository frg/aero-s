#ifndef _LINE2AXISOMMER_H_
#define _LINE2AXISOMMER_H_ 

#include <Utils.d/MyComplex.h>
#include <Element.d/Sommerfeld.d/SommerElement.h>

template <class Scalar> class GenFullSquareMatrix;
typedef GenFullSquareMatrix<double> FullSquareMatrix;
typedef GenFullSquareMatrix<DComplex> FullSquareMatrixC;

class Line2AxiSommer: public SommerElement {

	int nn[3];
	int type;
        double surfR0, surfZ0;

public:
	Line2AxiSommer(int, int, int);
	int getElementType() const override { return 12; };

        void setType(int t);
        void setSurf(double aR, double aZ);

        void renum(const int *) override;
        void renum(EleRenumMap&) override;

        FullSquareMatrix sommerMatrix(CoordSet&) const override;
        FullSquareMatrix sommerMatrix(CoordSet&, double *) const override;

        FullSquareMatrixC turkelMatrix(CoordSet&, double, int) override;
        FullSquareMatrixC turkelMatrix(CoordSet&, double, int, DComplex *) override;

        FullSquareMatrix interfMatrixConsistent(CoordSet&) override;
        FullSquareMatrix interfMatrixConsistent(CoordSet&, double*) override;
        FullSquareMatrix interfMatrixLumped(CoordSet&) override;
        FullSquareMatrix interfMatrixLumped(CoordSet&, double*) override;

        int* dofs(DofSetArray &, int *p=0) const override;
        int  numDofs() const override;
	int* nodes(int* = 0) const override;
        int  numNodes() const override;

        int getNode(int nd) const override { return nn[nd]; }
        const int *getNodes() const override { return nn; }
        int *getNodes() override { return nn; }

        void ffpAxiNeum(int, DComplex *, CoordSet &, DComplex **, double,
                     double(*)[3], double*, int);
        void ffpAxiDir(int, DComplex *, CoordSet &, DComplex **, double,
                     double(*)[3], double*, int);

};

#endif
