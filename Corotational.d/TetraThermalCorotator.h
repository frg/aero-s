#ifndef _TET_THERMAL_COROTATOR_H_
#define _TET_THERMAL_COROTATOR_H_

#include <Element.d/Element.h>
#include <Corotational.d/Corotator.h>

class MFTTData;

class TetraThermalCorotator : public Corotator {
     int order;
     int nn[4]; // currently only 4 node tetra is supported for nonlinear
     MFTTData *cctt;
     MFTTData *sctt;
     MFTTData *dctt;
     double Q;
     double rho;

   public:
     TetraThermalCorotator(int nn[4], CoordSet &, MFTTData *, MFTTData *, MFTTData *, double, double);

     void getStiffAndForce(GeomState &gs, CoordSet &cs,
                           FullSquareMatrix &elk, double *f, double dt, double t);

     void getInternalForce(GeomState &gs, CoordSet &cs,
                           FullSquareMatrix &elk, double *f, double dt, double t);

     bool useDefaultInertialStiffAndForce();

     void getInertialStiffAndForce(GeomState *refState, GeomState& c1, CoordSet& c0,
                                   FullSquareMatrix &elK, double *f, double dt, double t,
                                   double beta, double gamma, double alphaf, double alpham);

   private:
     void formTangentStiffness12(double ak, double bk, CoordSet &cs, double xn[4], double vn[4],
                                 FullSquareMatrix &K);

     void formInternalForce12(double ak, double bk, CoordSet &cs, double xn[4], double vn[4],
                              double *f);

     void formTangentStiffness34(double ac, double bc, double ad, double bd, CoordSet &cs,
                                 double xn[4], double vn[4], double s1, FullSquareMatrix &K);

     void formInternalForce34(double ac, double bc, double ad, double bd, CoordSet &cs,
                              double xn[4], double vn[4], double *f);

};

#endif

