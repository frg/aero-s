#include <Corotational.d/TetraThermalCorotator.h>
#include <Corotational.d/GeomState.h>
#include <Corotational.d/utilities.h>
#include <Element.d/Element.h>
#include <Element.d/Tetra.d/ThermIsoParamTetra.h>
#include <Element.d/Helm.d/IsoParamUtils.h>
#include <Element.d/Helm.d/GaussRules.h>
#include <Math.d/FullSquareMatrix.h>
#include <Math.d/matrix.h>
#include <Utils.d/dofset.h>
#include <Utils.d/dbg_alloca.h>
#include <Utils.d/linkfc.h>
#include <Utils.d/pstress.h>
#include <Utils.d/MFTT.h>

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <alloca.h>

/************************************************************************************
*
* Purpose :
*  Compute tangent stiffness matrix and internal force vector
*  of the temperature dependent heat equation
*
* Input:
* xn    : current nodal temperature vector
* xnp   : previous nodal temperature vector
*
* Output:
* kt    : tangent stiffness matrix
*
*
* Written by: Steven N. Rodriguez
*
* Date: April 2020
*
************************************************************************************/

TetraThermalCorotator::TetraThermalCorotator(int _n[4], CoordSet &cs, MFTTData *_cctt, MFTTData *_sctt, MFTTData *_dctt,
                                             double _Q, double _rho)
{
  order = 2;

  // Node number
  nn[0] = _n[0];
  nn[1] = _n[1];
  nn[2] = _n[2];
  nn[3] = _n[3];

  cctt = _cctt; // Conductivity temperature table
  sctt = _sctt; // Specific heat temperature table
  dctt = _dctt; // Density temperature table

  Q = _Q;       // Specific heat used to compute initial mass matrix
  rho = _rho;   // Density used to compute initial mass matrix
}

void
TetraThermalCorotator::getStiffAndForce(GeomState &ts, CoordSet &cs,
                                        FullSquareMatrix &K, double *f, double dt, double t)
{
  double xn[4], vn[4];

  // Get current temperature geomstate
  NodeState &tn1 = ts[nn[0]];
  NodeState &tn2 = ts[nn[1]];
  NodeState &tn3 = ts[nn[2]];
  NodeState &tn4 = ts[nn[3]];

  // Set current temperature state
  xn[0] = tn1.x; // temperature of node 1
  xn[1] = tn2.x; // temperature of node 2
  xn[2] = tn3.x; // temperature of node 3
  xn[3] = tn4.x; // temperature of node 4

  // Set current temperature first time derivative
  vn[0] = tn1.v[0]; // temperature of node 1
  vn[1] = tn2.v[0]; // temperature of node 2
  vn[2] = tn3.v[0]; // temperature of node 3
  vn[3] = tn4.v[0]; // temperature of node 4

  // Get temperature table interpolant coefficients
  double theta_n = (xn[0]+xn[1]+xn[2]+xn[3])/4;
  double ak, bk, ac, bc, ad, bd;
  cctt->getSlopeAndIntAlt(theta_n, &ak, &bk);
  sctt->getSlopeAndIntAlt(theta_n, &ac, &bc);
  dctt->getSlopeAndIntAlt(theta_n, &ad, &bd);

  formTangentStiffness12(ak, bk, cs, xn, vn, K);

  formInternalForce12(ak, bk, cs, xn, vn, f);
}

void TetraThermalCorotator::getInternalForce(GeomState &ts, CoordSet &cs,
                                             FullSquareMatrix &, double *f, double dt, double t)
{
  double xn[4], vn[4];

  // Get current temperature geomstate
  NodeState &tn1 = ts[nn[0]];
  NodeState &tn2 = ts[nn[1]];
  NodeState &tn3 = ts[nn[2]];
  NodeState &tn4 = ts[nn[3]];

  // Set current temperature state
  xn[0] = tn1.x; // temperature of node 1
  xn[1] = tn2.x; // temperature of node 2
  xn[2] = tn3.x; // temperature of node 3
  xn[3] = tn4.x; // temperature of node 4

  // Set current temperature first time derivative
  vn[0] = tn1.v[0]; // first derivative of temperature of node 1
  vn[1] = tn2.v[0]; // first derivative of temperature of node 2
  vn[2] = tn3.v[0]; // first derivative of temperature of node 3
  vn[3] = tn4.v[0]; // first derivative of temperature of node 4

  // Get temperature table interpolant coefficients
  double theta_n = (xn[0]+xn[1]+xn[2]+xn[3])/4;
  double ak, bk;
  cctt->getSlopeAndIntAlt(theta_n, &ak, &bk);

  formInternalForce12(ak, bk, cs, xn, vn, f);
}

void
TetraThermalCorotator::formTangentStiffness12(double a_k, double b_k, CoordSet &cs, double xn[4], double vn[4],
                                              FullSquareMatrix &K)
{
  // Initializing components in the tangent stiffness matrix
  double Kt1[4][4] = {0};
  double Kt2[4][4] = {0};
  double Kt3[4][4] = {0};
  double Kt4[4][4] = {0};
  double Kt5[4][4] = {0};

  // Initializing utilities and coordinates
  IsoParamUtilsTetra ipu(order);
  int orderc = ipu.getorderc();
  double *xyz=(double*)dbg_alloca(sizeof(double)*3*orderc);
  cs.getCoordinates(nn,orderc,xyz,xyz+orderc,xyz+2*orderc);

  // Initializing tangent stiffness matrix function
  GalTangentStiffFunction12<double> f(orderc);
  int gorder = 7*7*7;
  if(order<=3) gorder = 4*4*4;

  // Computing tangent stiffness matrix components
  ipu.volumeInt3d_TSM(xyz, f, xn, vn, Kt1, Kt2, Kt3, Kt4, Kt5, gorder);

  // Component 1: b_k*d/dT(B'*B*T)
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt1[i][j] *= b_k;
    }
  }

  // Component 2: a_k*d/dT(B'*N*T*B*T)
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt2[i][j] *= a_k;
    }
  }

  // Copy tangent stiffness matrix to element K matrix
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      K[i][j] = Kt1[i][j] + Kt2[i][j];
    }
  }
}

void
TetraThermalCorotator::formInternalForce12(double a_k, double b_k, CoordSet &cs, double xn[4], double vn[4],
                                           double *f) {
  // Initialize internal force vectors
  double f1[4] = {0};
  double f2[4] = {0};
  double f3[4] = {0};

  // Initializing utilities and coordinates
  IsoParamUtilsTetra ipu(order);
  int orderc = ipu.getorderc();
  double *xyz=(double*)dbg_alloca(sizeof(double)*3*orderc);
  cs.getCoordinates(nn,orderc,xyz,xyz+orderc,xyz+2*orderc);

  // Initializing internal force function
  GalInternalForceFunction12<double> f_func(orderc);
  int gorder = 7*7*7;
  if(order<=3) gorder = 4*4*4;

  // Computing internal force vector
  ipu.volumeInt3d_IFV(xyz, f_func, xn, vn, f1, f2, f3, gorder);

  // Component 1: b_k*(B'*B*T)
  for(int i=0; i<orderc; i++) {
    f1[i] *= b_k;
  }

  // Component 2: a_k*(B'*N*T*B*T)
  for(int i=0; i<orderc; i++) {
    f2[i] *= a_k;
  }

  // Copy internal force to element f matrix
  for(int i=0; i<orderc; i++) {
    f[i] = f1[i] + f2[i];
  }
}

bool TetraThermalCorotator::useDefaultInertialStiffAndForce() {
 return false;
}

void TetraThermalCorotator::getInertialStiffAndForce(GeomState *refState, GeomState& ts, CoordSet& cs,
                                                     FullSquareMatrix &K, double *f, double dt, double t,
                                                     double beta, double gamma, double alphaf, double alpham) {
  double xnref[4], vnref[4], xn[4], vn[4];

  // Get reference state
  NodeState &tn1ref = (*refState)[nn[0]];
  NodeState &tn2ref = (*refState)[nn[1]];
  NodeState &tn3ref = (*refState)[nn[2]];
  NodeState &tn4ref = (*refState)[nn[3]];

  // Set reference temperature state
  xnref[0] = tn1ref.x; // temperature of node 1
  xnref[1] = tn2ref.x; // temperature of node 2
  xnref[2] = tn3ref.x; // temperature of node 3
  xnref[3] = tn4ref.x; // temperature of node 4

  // Set reference temperature first time derivative
  vnref[0] = tn1ref.v[0]; // first derivative of temperature of node 1
  vnref[1] = tn2ref.v[0]; // first derivative of temperature of node 2
  vnref[2] = tn3ref.v[0]; // first derivative of temperature of node 3
  vnref[3] = tn4ref.v[0]; // first derivative of temperature of node 4

  // Get current state
  NodeState &tn1 = ts[nn[0]];
  NodeState &tn2 = ts[nn[1]];
  NodeState &tn3 = ts[nn[2]];
  NodeState &tn4 = ts[nn[3]];

  // Set current temperature state
  xn[0] = tn1.x; // temperature of node 1
  xn[1] = tn2.x; // temperature of node 2
  xn[2] = tn3.x; // temperature of node 3
  xn[3] = tn4.x; // temperature of node 4

  // Set current temperature first time derivative
  double s1 = gamma/(dt*beta), s2 = (1-(1-alphaf)*gamma/beta);
  vn[0] = s1*(xn[0]-xnref[0]) + s2*vnref[0]; // first derivative of temperature of node 1
  vn[1] = s1*(xn[1]-xnref[1]) + s2*vnref[1]; // first derivative of temperature of node 2
  vn[2] = s1*(xn[2]-xnref[2]) + s2*vnref[2]; // first derivative of temperature of node 3
  vn[3] = s1*(xn[3]-xnref[3]) + s2*vnref[3]; // first derivative of temperature of node 4

  // Get temperature table interpolant coefficients
  double theta_n = (xn[0]+xn[1]+xn[2]+xn[3])/4;
  double ac, bc, ad, bd;
  sctt->getSlopeAndIntAlt(theta_n, &ac, &bc);
  dctt->getSlopeAndIntAlt(theta_n, &ad, &bd);

  formTangentStiffness34(ac, bc, ad, bd, cs, xn, vn, s1, K);

  formInternalForce34(ac, bc, ad, bd, cs, xn, vn, f);
}

void
TetraThermalCorotator::formTangentStiffness34(double a_c, double b_c, double a_d, double b_d, CoordSet &cs,
                                              double xn[4], double vn[4], double s1, FullSquareMatrix &K)
{
  // Initializing components in the tangent stiffness matrix
  double Kt3[4][4] = {0};
  double Kt4[4][4] = {0};
  double Kt5[4][4] = {0};
  double Kt6[4][4] = {0};
  double Kt7[4][4] = {0};

  // Initializing utilities and coordinates
  IsoParamUtilsTetra ipu(order);
  int orderc = ipu.getorderc();
  double *xyz=(double*)dbg_alloca(sizeof(double)*3*orderc);
  cs.getCoordinates(nn,orderc,xyz,xyz+orderc,xyz+2*orderc);

  // Initializing tangent stiffness matrix function
  GalTangentStiffFunction34<double> f(orderc);
  int gorder = 7*7*7;
  if(order<=3) gorder = 4*4*4;

  // Computing tangent stiffness matrix components
  ipu.volumeInt3d_TSM(xyz, f, xn, vn, Kt3, Kt4, Kt5, Kt6, Kt7, gorder);

  // Component 3: a_d*a_c*d/dT(N'*N*T*N*T*N*\dot{T})
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt3[i][j] *= a_d*a_c;
    }
  }

  // Component 4: (a_d*b_c+a_c*b_d)*d/dT(N'*N*T*N*\dot{T})
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt4[i][j] *= a_d*b_c + a_c*b_d;
    }
  }

  // Component 5: s1*(b_d*b_c-rho*Q)*d/d[\dot{T}](N'*N*\dot{T})
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt5[i][j] *= s1*(b_d*b_c - rho*Q);
    }
  }

  // Component 6: s1*a_d*a_c*d/d[\dot{T}](N'*N*T*N*T*N*\dot{T})
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt6[i][j] *= s1*a_d*a_c;
    }
  }

  // Component 7: s1*(a_d*b_c+a_c*b_d)*d/d[\dot{T}](N'*N*T*N*\dot{T})
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      Kt7[i][j] *= s1*(a_d*b_c + a_c*b_d);
    }
  }

  // Copy tangent stiffness matrix to element K matrix
  for(int j=0; j<orderc; j++) {
    for(int i=0; i<orderc; i++) {
      K[i][j] = Kt3[i][j] + Kt4[i][j] + Kt5[i][j] + Kt6[i][j] + Kt7[i][j];
    }
  }
}

void
TetraThermalCorotator::formInternalForce34(double a_c, double b_c, double a_d, double b_d, CoordSet &cs,
                                           double xn[4], double vn[4], double *f) {
  // Initialize internal force vectors
  double f3[4] = {0};
  double f4[4] = {0};
  double f5[4] = {0};

  // Initializing utilities and coordinates
  IsoParamUtilsTetra ipu(order);
  int orderc = ipu.getorderc();
  double *xyz=(double*)dbg_alloca(sizeof(double)*3*orderc);
  cs.getCoordinates(nn,orderc,xyz,xyz+orderc,xyz+2*orderc);

  // Initializing internal force function
  GalInternalForceFunction34<double> f_func(orderc);
  int gorder = 7*7*7;
  if(order<=3) gorder = 4*4*4;

  // Computing internal force vector
  ipu.volumeInt3d_IFV(xyz, f_func, xn, vn, f3, f4, f5, gorder);

  // Component 3: (b_d*b_c-rho*Q)*N'*N*\dot{T}
  for(int i=0; i<orderc; i++) {
    f3[i] *= b_d*b_c - rho*Q;
  }

  // Component 4: a_d*a_c*N'*N*T*N*T*N*\dot{T}
  for(int i=0; i<orderc; i++) {
    f4[i] *= a_d*a_c;
  }

  // Component 5: (a_d*b_c+a_c*b_d)*N'*N*T*N*\dot{T}
  for(int i=0; i<orderc; i++) {
    f5[i] *= a_d*b_c + a_c*b_d;
  }

  // Copy internal force to element f matrix
  for(int i=0; i<orderc; i++) {
    f[i] = f3[i] + f4[i] + f5[i];
  }
}
