#ifndef FEM_AUGMENTEDGMRES_H
#define FEM_AUGMENTEDGMRES_H

#include <vector>
#include <functional>
#include <Eigen/Dense>
#include <Feti.d/DistrVector.h>

struct ResStats {
  int niter;
  double resid;
};

template <typename Scalar>
class AugmentedGMRES {
public:
  using Vec = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
  using Mat = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

  using VView  = VectorView<Scalar>;
  using CVView = VectorView<const Scalar>;
  using DVec   = GenDistrVector<Scalar>;

  using Operator  = std::function<void(const DVec&, DVec&)>;
  using Gatherer  = std::function<void(const DVec&, VView)>;
  using Scatterer = std::function<void(CVView, DVec&)>;
  using Unifier   = std::function<void(gsl::span<Scalar>)>;

  AugmentedGMRES(bool output, double eps, int maxits, int numVec, 
                 int deflationSize, int deflationType, bool multipleLhs,
                 int nmThreads, int halfSize, Operator A, Operator M,
                 const DistrInfo &info, Gatherer gatherer,
                 Scatterer scatterer, Unifier unifier);
  ~AugmentedGMRES();

  int solve(const GenDistrVector<Scalar> &b, GenDistrVector<Scalar> &x, double refNorm = 0.0);
  void reset();
  double getResid() { return history.back().resid; }

private:
  // whether to print to screen on this mpi process (typically set to true on root process only)
  bool output;
  // whether to perform one additional matrix-vector product to compute true final residual
  bool checkFinalRes = false;
  // suppress printing to screen of relative error in last iteration
  bool printConverged = false;
  // reset deflation after lhs update (false) or adapt subspace to new lhs (true)
  bool multipleLhs;
  // size of Krylov subspace (excluding deflation)
  int numVec;
  // maximum number of iterations
  int maxits;
  // size of deflation subspace
  int deflationSize;
  // whether to deflate smallest (0) or largest (1) eigenvalues
  int deflationType;
  // number of threads
  int nThreads;
  // relative tolerance for the preconditioned residual
  double eps;
  // size needed for all the master portion of interface vectors
  int halfSize;
  // number of rhs solves since lhs update
  int rhsIndex = 0;

  // the operator
  Operator A;
  // the preconditioner
  Operator M;
  // sum the data across processors
  Unifier unify;
  // operator extracting the local master part of a distributed vector
  Gatherer gather;
  // operator recreating a distributed vector from the master version
  Scatterer scatter;
  // matrix space for V
  Mat Vspace;
  // matrix space for H
  Mat Hspace;
  // space for R such that H = P R
  Mat Rspace;
  // set at the end of a solution to the number of valid vectors in Q
  int krylovSize;
  // vector of Givens rotations
  std::vector<Eigen::JacobiRotation<Scalar>> givens;
  // deflation direction matrix
  Mat U;
  // unit matrix C = M A U, C^H C = I
  Mat C;
  // B = C^H Q
  Mat Bspace;
  // convergence history for stats printing
  std::vector<ResStats> history;
  // work data
  GenDistrVector<Scalar> rd, zd, vd, wd;
  Vec gs, v;

  void buildDeflation(int typePrec, int iter);
  double unifiedNorm(VectorView<Scalar> v);
  int iterate(double beta, int startIt, double refRes, int typePrec, int& exitloop, double& error);
  void subspaceSolve(GenDistrVector<Scalar> &b, GenDistrVector<Scalar> &x, int typePrec);
};

#endif
