#include "AugmentedGMRES.h"

#include <Eigen/Eigenvalues>
#include <algorithm>
#include <complex>
#include <numeric>

#ifdef EIGEN_USE_MKL_ALL
#include <mkl.h>
void setBLASNumThreads(int n) { mkl_set_num_threads_local(n); }
#else
void setBLASNumThreads(int n) {}
#endif

template<typename Scalar>
AugmentedGMRES<Scalar>::AugmentedGMRES(bool output, double eps, int maxits, int numVec,
                                       int deflationSize, int deflationType, bool multipleLhs,
                                       int nmThreads, int halfSize, Operator A, Operator M,
                                       const DistrInfo &info, Gatherer gatherer,
                                       Scatterer scatterer, Unifier unifier)
 : output(output), eps(eps), maxits(maxits), numVec(numVec), deflationSize(deflationSize),
   deflationType(deflationType), multipleLhs(multipleLhs), nThreads(nmThreads), halfSize(halfSize),
   A(A), M(M), unify(unifier), gather(gatherer), scatter(scatterer), Vspace(halfSize, numVec+1),
   Hspace(numVec+1, numVec), Rspace(numVec+1, numVec), krylovSize(0), rd(info), zd(info), vd(info),
   wd(info), gs(numVec+1), v(halfSize), U(halfSize, 0), C(halfSize, 0) {
}

template<typename Scalar>
AugmentedGMRES<Scalar>::~AugmentedGMRES() {
}

template<typename Scalar>
void AugmentedGMRES<Scalar>::reset() {
  rhsIndex = 0;
  if(!multipleLhs) {
    C.resize(0,0);
    krylovSize = 0;
  }
  history.clear();
}

template<typename Scalar>
double AugmentedGMRES<Scalar>::unifiedNorm(VectorView<Scalar> v) {
  Scalar v2 = v.squaredNorm();
  unify({&v2, 1});
  return std::sqrt(std::real(v2));
}

template<typename Scalar>
int
AugmentedGMRES<Scalar>::solve(const GenDistrVector<Scalar> &b,
                              GenDistrVector<Scalar> &x,
                              double refNorm) {
  int typePrec = 1;
  double beta, l2res, res0;
  int iter = 0;
  int exitLoop = 0;
  setBLASNumThreads(nThreads);
  x.zero(); rd = b; // initialize solution and residual
  do {
    if(deflationSize > 0) buildDeflation(typePrec, iter);
    if(iter > 0) {
      A(x, wd);
      rd = b - wd;
    }
    if(typePrec == 1) {
      M(rd, wd);
      rd = wd;
    }
    beta = rd.norm();
    if(iter == 0) {
      if(refNorm != 0) {
        res0 = refNorm;
      }
      else if(typePrec == 1) {
        M(b, wd);
        res0 = wd.norm();
      }
      else { 
        res0 = beta;
      }
    }
    else if(output) {
      fprintf(stderr, " *** Krylov Space Full - Restarting GMRES \n");
    }
    if(beta != 0 && C.cols() > 0) {
      subspaceSolve(rd, x, typePrec);
      beta = rd.norm();
    }
    if(beta < eps*res0) {
      if(output && printConverged) fprintf(stderr, "%4d %47.6e\n", 0, beta/res0);
      history.push_back({0, beta});
      rhsIndex++;
      krylovSize = 0;
      return 0;
    }

    Hspace.setZero(); // to guarantee we have zeros in the lower part
    gather(rd, {Vspace.col(0).data(), Vspace.rows()});
    Vspace.col(0) /= beta;
    zd = rd;
    zd /= beta;

    iter += iterate(beta, iter, res0, typePrec, exitLoop, l2res);
    if(iter == maxits) exitLoop = 1;

    // build the solution
    auto g = gs.topRows(krylovSize);
    Rspace.block(0,0,krylovSize,krylovSize).template triangularView<Eigen::Upper>().solveInPlace(g);
    v = Vspace.leftCols(krylovSize) * g;
    if(C.cols() > 0) {
      Vec d(C.cols());
      d = Bspace.leftCols(krylovSize) * g; // B and g are unified already.
      v -= U * d;
    }

    scatter(v, vd);
    if(typePrec == 2) {
      M(vd, wd);
      vd = wd;
    }
    x += vd;
  }
  while(exitLoop == 0);
  if(checkFinalRes) {
    // compute final residual
    A(x, wd);
    rd = b - wd;
    if(typePrec == 1) {
      M(rd, wd);
      rd = wd;
    }
    history.push_back({iter, rd.norm()});
  }
  else {
    history.push_back({iter, l2res});
  }
  rhsIndex++;
  return iter;
}

template <typename Scalar>
int
AugmentedGMRES<Scalar>::iterate(double beta, int startIt, double refRes, int typePrec, int& exitloop, double& error) {
  krylovSize = 0;
  gs.setZero();
  gs[0] = beta;
  auto nC = C.cols();
  Bspace.resize(nC, numVec);
  int iter;
  for(iter = 0; iter < numVec && iter + startIt < maxits; ++iter) {
    int n = iter+1;
    switch(typePrec) {
      case 0: {
        A(zd, vd);
      } break;
      case 1: {
        A(zd, wd);
        M(wd, vd);
      } break;
      case 2: {
        M(zd, wd);
        A(wd, vd);
      } break;
    }
    gather(vd, v);
    if(nC > 0) {
      Vec y(nC);
      y = C.adjoint() * v;
      unify({y.data(), y.size()});
      v -= C * y;
      Bspace.col(iter) = y;
    }
    auto Q = Vspace.leftCols(n);
    Vec y(n);
    auto h = Hspace.col(iter).topRows(n);
    h.setZero();
    // two iterations of classical Gram Schmidt for stability (see Giraud et al. 2005)
    for(int i = 0; i < std::min(n, 2); ++i) {
      y = Q.adjoint() * v;
      unify({y.data(), y.size()}); // global operation
      h += y;
      v -= Q * y;
    }
    auto hp = Hspace.col(iter).topRows(n+1);

    hp[n] = unifiedNorm(v);

    v *= 1.0/hp[n];
    Vspace.col(iter+1) = v;
    auto r = Rspace.col(iter).topRows(n+1);
    r = hp;

    // perform the previous rotations
    for(int i = 0; i < iter; ++i) {
      r.applyOnTheLeft(i, i+1, givens[i].adjoint());
    }
    givens.emplace_back();
    givens[iter].makeGivens(r[iter], r[iter+1]);
    r.applyOnTheLeft(iter, iter+1, givens[iter].adjoint());
    gs.applyOnTheLeft(iter, iter+1, givens[iter].adjoint());
    krylovSize = n;

    error = std::abs(gs[n]);
    bool converged = ((error <= eps * refRes) || (n + startIt == maxits));
    if(output && (!converged || printConverged)) {
      fprintf(stderr, "%4d %47.6e\n", startIt+iter, error/refRes);
    }
    if(converged) {
      exitloop = 1;
      break;
    }

    scatter(v, zd);
  }
  return iter;
}

template <typename Scalar>
void AugmentedGMRES<Scalar>::subspaceSolve(GenDistrVector<Scalar> &r,
                                           GenDistrVector<Scalar> &x,
                                           int typePrec) {
  // y = C^H*r
  Vec y(C.cols());
  gather(r, v);
  y = C.adjoint() * v;
  unify({y.data(), y.size()});

  // 7: r1 = r0 - C*C^H*r0
  v -= C * y;
  scatter(v, r);

  // 6: x1 = x0 + U*C^H*r0
  v = U * y;
  scatter(v, vd);
  if(typePrec == 2) {
    M(vd, wd);
    vd = wd;
  }
  x += vd;
}

template<typename Scalar>
void
compute(Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& A,
        Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& P, int deflationType) {
  // compute eigenvectors associated with smallest (or largest) magnitude eigenvalues of real matrix A and store in P
  // using real representation of complex conjugate pairs
  Eigen::EigenSolver<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> eigenSolver(A);
  auto e = eigenSolver.eigenvalues().eval();
  auto data = e.cwiseAbs();
  std::vector<int> index(data.size());
  std::iota(index.begin(), index.end(), 0);
  if(deflationType == 0) // sort from smallest to largest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] < data[b]); });
  else                   // sort from largest to smallest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] > data[b]); });
  auto conjugate_pair = [](std::complex<Scalar>& a, std::complex<Scalar>& b) -> bool {
    return (a.real() == b.real() && a.imag() == -b.imag());
  };
  for(int i = 0; i < P.cols(); ++i) {
    if(i > 0 && conjugate_pair(e[index[i-1]], e[index[i]])) {
      P.col(i) = eigenSolver.eigenvectors().col(index[i]).imag();
    }
    else {
      P.col(i) = eigenSolver.eigenvectors().col(index[i]).real();
      if(i == P.cols()-1 && i < index.size()-1 && conjugate_pair(e[index[i]], e[index[i+1]])) {
        P.conservativeResize(P.rows(), P.cols()+1);
        P.col(i+1) = eigenSolver.eigenvectors().col(index[i+1]).imag();
        break;
      }
    }
  }
}

template<typename Scalar>
void
compute(Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& A,
        Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& B,
        Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& P, int deflationType) {
  // compute eigenvectors associated with smallest (or largest) magnitude eigenvalues of real matrices (A, B) and store in P
  // using real representation of complex conjugate pairs
  Eigen::GeneralizedEigenSolver<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> eigenSolver(A, B);
  auto e = eigenSolver.eigenvalues().eval();
  auto data = e.cwiseAbs();
  std::vector<int> index(data.size());
  std::iota(index.begin(), index.end(), 0);
  if(deflationType == 0) // sort from smallest to largest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] < data[b]); });
  else                   // sort from largest to smallest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] > data[b]); });
  auto conjugate_pair = [](std::complex<Scalar>& a, std::complex<Scalar>& b) -> bool {
    return (a.real() == b.real() && a.imag() == -b.imag());
  };
  for(int i = 0; i < P.cols(); ++i) {
    if(i > 0 && conjugate_pair(e[index[i-1]], e[index[i]])) {
      P.col(i) = eigenSolver.eigenvectors().col(index[i]).imag();
    }
    else {
      P.col(i) = eigenSolver.eigenvectors().col(index[i]).real();
      if(i == P.cols()-1 && i < index.size()-1 && conjugate_pair(e[index[i]], e[index[i+1]])) {
        P.conservativeResize(P.rows(), P.cols()+1);
        P.col(i+1) = eigenSolver.eigenvectors().col(index[i+1]).imag();
        break;
      }
    }
  }
}

template<typename Scalar>
void
compute(Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>& A,
        Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>& P, int deflationType) {
  // compute eigenvectors associated with smallest (or largest) magnitude eigenvalues of complex matrix A and store in P
  Eigen::ComplexEigenSolver<Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>> eigenSolver(A);
  auto e = eigenSolver.eigenvalues().eval();
  auto data = e.cwiseAbs();
  std::vector<int> index(data.size());
  std::iota(index.begin(), index.end(), 0);
  if(deflationType == 0) // sort from smallest to largest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] < data[b]); });
  else                   // sort from largest to smallest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] > data[b]); });
  for(int i = 0; i < P.cols(); ++i) {
    P.col(i) = eigenSolver.eigenvectors().col(index[i]);
  }
}

template<typename Scalar>
void
compute(Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>& A,
        Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>& B,
        Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>& P, int deflationType) {
  // compute eigenvectors associated with smallest (or largest) magnitude eigenvalues of complex matrices (A, B) and store in P
  // note: there is currently no generalized eigensolver for complex matrices in the Eigen library, so here
  //       we alternatively solve the standard eigenvalue problem (A^{-1}*B)*v = (1/λ)*I*v
  //       where λ are the eigenvalues of the generalized eigenvalue problem A*v = λ*B*v
  A.ldlt().solveInPlace(B);
  Eigen::ComplexEigenSolver<Eigen::Matrix<std::complex<Scalar>, Eigen::Dynamic, Eigen::Dynamic>> eigenSolver(B);
  auto e = eigenSolver.eigenvalues().eval();
  auto data = e.cwiseInverse().cwiseAbs();
  std::vector<int> index(data.size());
  std::iota(index.begin(), index.end(), 0);
  if(deflationType == 0) // sort from smallest to largest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] < data[b]); });
  else                   // sort from largest to smallest
    std::sort(index.begin(), index.end(), [&](const int& a, const int& b) { return (data[a] > data[b]); });
  for(int i = 0; i < P.cols(); ++i) {
    P.col(i) = eigenSolver.eigenvectors().col(index[i]);
  }
}

template <typename Scalar>
void AugmentedGMRES<Scalar>::buildDeflation(int typePrec, int iter) {
  int nVec;
  // Assuming it's the first time
  if(C.cols() == 0 && krylovSize > 0) {
    int n = krylovSize;
    nVec = std::min(deflationSize, n);
    auto Hb = Hspace.topLeftCorner(n+1, n);
    Mat S = Hspace.topLeftCorner(n, n);
    Vec en = Vec::Unit(n, n-1);
    S += (Hspace(n, n-1) * Hspace(n, n-1)) * S.adjoint().lu().solve(en*en.adjoint());

    // 14: compute eigenvectors of S corresponding to nVec smallest eigenvalues and store in P
    Mat P(n, nVec);
    compute(S, P, deflationType);
    nVec = P.cols(); // P can be resized by +1 to accomodate imaginary part of complex eigenvector

    // 16: reduced QR factorization of H_m * P
    auto qrDec = (Hb * P).householderQr();
    Mat Q = qrDec.householderQ() * Mat::Identity(n+1, nVec);

    // 17: C = V_{m+1} * Q
    C = Vspace.leftCols(n+1) * Q;

    // 18: U = \tilde{Y} * R^{-1} = V_m * P * R^{-1}
    auto R = qrDec.matrixQR().topLeftCorner(nVec, nVec).template triangularView<Eigen::Upper>();
    Mat Rinv = R.solve(Mat::Identity(nVec, nVec));
    U = Vspace.leftCols(n) * P * Rinv;
  }
  else if(C.cols() > 0 && krylovSize > 0) {
    int nC = C.cols();
    int nV = krylovSize;
    int nTot = nC + nV;
    nVec = std::min(deflationSize, nTot);

    // 24: \hat{V}_m = [ \tilde{U}_k  V_{m-k} ]
    Mat Vhat(Vspace.rows(), nTot);
    Vhat.leftCols(nC) = U; // these columns are normalized below
    Vhat.rightCols(nV) = Vspace.leftCols(nV);

    // 25: \hat{W}_{m+1} = [ C_k  V_{m-k+1} ]
    Mat What(Vspace.rows(), nTot+1);
    What.leftCols(nC) = C;
    What.rightCols(nV+1) = Vspace.leftCols(nV+1);

    // 26: G_m = [ D_k  B_{m-k} ]
    //           [      H_{m-k} ]
    Mat G(nTot+1, nTot);
    G.setZero();
    G.block(0, nC, nC, nV)    = Bspace.topLeftCorner(nC, nV);
    G.block(nC, nC, nV+1, nV) = Hspace.topLeftCorner(nV+1, nV);
    for(int i = 0; i < nC; ++i) {
      double d = 1.0 / unifiedNorm({Vhat.col(i).data(), Vhat.rows()});
      Vhat.col(i) *= d;
      G(i, i) = d;
    }

    // 30: compute eigenvectors associated with smallest magnitude eigenvalues of (PenA, PenB) and store in P
    //     where PenA = G^H*G, PenB = G^H*\hat{W}^H*\hat{V}
    Mat PenA = G.adjoint() * G;
    Mat F = What.adjoint() * Vhat;
    unify({F.data(), F.size()});
    Mat PenB = G.adjoint() * F;
    Mat P(nTot, nVec);
    compute(PenA, PenB, P, deflationType);
    nVec = P.cols(); // P can be resized by +1 to accomodate imaginary part of complex eigenvector

    // 32: compute reduced (thin) QR factorization of G*P
    auto qrDec = (G * P).householderQr();
    Mat Q = qrDec.householderQ() * Mat::Identity(nTot+1, nVec);

    // 33: C = \hat{W}*Q
    C = What * Q;

    // 34: U = \tilde{Y}*R^{-1} = \hat{V}*P*R^{-1}
    auto R = qrDec.matrixQR().topLeftCorner(nVec, nVec).template triangularView<Eigen::Upper>();
    Mat Rinv = R.solve(Mat::Identity(nVec, nVec));
    U = Vhat * P * Rinv;
  }
  else {
    int nC = C.cols();
    nVec = std::min(deflationSize, nC);
  }

  if(multipleLhs && rhsIndex == 0 && iter == 0 && nVec > 0) {
    if(output) std::cerr << "activating multipleLhs, nVec = " << nVec << std::endl;
    // lambda function for compute matrix-vector product
    auto Op = [this,&typePrec](const Eigen::Ref<const Vec> p, Eigen::Ref<Vec> Ap) {
      scatter({p.data(), p.size()}, zd);
      switch(typePrec) {
        case 0: {
          A(zd, vd);
        } break;
        case 1: {
          A(zd, wd);
          M(wd, vd);
        } break;
        case 2: {
          M(zd, wd);
          A(wd, vd);
        } break;
      }
      gather(vd, {Ap.data(), Ap.size()});
    };
    // 36. \tilde{Y} = U
    // compute A*\tilde{Y}
    Mat AY(U.rows(), nVec);
    for(int i = 0; i < nVec; ++i) {
      Op(U.col(i), AY.col(i));
    }
    if(false/*com->size() == 1*/) {
      // 3: compute reduced (thin) QR factorization of A*\tilde{Y}
      auto qrDec = AY.householderQr();

      // 4: C = Q
      C = qrDec.householderQ() * Mat::Identity(U.rows(), nVec);

      // 5: U = \tilde{Y}*R^{-1}
      auto R = qrDec.matrixQR().topLeftCorner(nVec, nVec).template triangularView<Eigen::Upper>();
      Mat Rinv = R.solve(Mat::Identity(nVec, nVec));
      U = U * Rinv;
    }
    else {
      // 3: compute reduced (thin) QR factorization of A*\tilde{Y}
      Mat R(nVec, nVec);
      for(int i = 0; i < nVec; ++i) {
        for(int j = 0; j < i; ++j) {
          R(i,j) = 0.0;
        }
        double norm = unifiedNorm({AY.col(i).data(), AY.rows()});
        R(i,i) = norm;
        AY.col(i) /= norm;
        for(int j = i + 1; j < nVec; ++j) {
          Scalar r = AY.col(i).dot(AY.col(j));
          unify({&r, 1});
          AY.col(j) -= r * AY.col(i);
          R(i,j) = r;
        }
      }

      // 4: C = Q
      C = AY;

      // 5: U = \tilde{Y}*R^{-1}
      Mat Rinv = R.template triangularView<Eigen::Upper>().solve(Mat::Identity(nVec, nVec));
      U = U * Rinv;
    }
  }
}

template class AugmentedGMRES<double>;
template class AugmentedGMRES<std::complex<double>>;
