#ifndef _COMPRESSIBLEOGDENSTRAINENERGYDENSITYFUNCTION_H_
#define _COMPRESSIBLEOGDENSTRAINENERGYDENSITYFUNCTION_H_

#include <limits>
#include <Element.d/Function.d/StrainEnergyDensity.d/StrainEnergyDensityFunction.h>

namespace Simo {

template<typename Scalar>
class CompressibleOgdenStrainEnergyDensityFunction
: public StrainEnergyDensityFunction<Scalar>
{
    Eigen::Matrix<double,3,1> mu, alpha;
    Eigen::Matrix<double,2,1> K;

  public:
    CompressibleOgdenStrainEnergyDensityFunction(double mu1, double mu2, double mu3, double alpha1, double alpha2, double alpha3,
                                                 double K1, double K2)
    {
      mu << mu1, mu2, mu3;
      alpha << alpha1, alpha2, alpha3;
      K << K1, K2;
    }

    Scalar operator() (const Eigen::Matrix<Scalar,3,3> &F) const 
    {
      // inputs: deformation gradient
      // output: strain energy density
#if EIGEN_VERSION_AT_LEAST(3,4,0)
      return Scalar(0);
#elif EIGEN_VERSION_AT_LEAST(3,3,8)
      using std::exp;
      using std::pow;

      Eigen::Matrix<Scalar,3,3> C = F.transpose()*F; // right Cauchy-Green strain
      Eigen::Matrix<Scalar,3,1> lambda; // principal stretches

      if(F.isIdentity(100*std::numeric_limits<double>::epsilon())) {
        // this is a workaround because eigenvalues are not differentiable when F is the identity.
        lambda = C.diagonal().cwiseSqrt();
      }
      else {
        Eigen::SelfAdjointEigenSolver<Eigen::Matrix<Scalar,3,3> > es;
        es.computeDirect(C, Eigen::EigenvaluesOnly); // TODO: check if this function is auto-differentiable
        lambda = es.eigenvalues().cwiseSqrt();
      }

      Scalar J = F.determinant();
      Scalar U = K[0]/2*(J-1)*(J-1);
      if(K[1] != 0) U += K[1]/2*(J-1)*(J-1)*(J-1)*(J-1);

      Scalar ooJcbrt = pow(J,-1/3.);
      Scalar lambdabar[3] = { ooJcbrt*lambda[0], ooJcbrt*lambda[1], ooJcbrt*lambda[2] };
      Scalar W = 0;
      for(int i = 0; i < 3; ++i) {
        if(mu[i] != 0) W += mu[i]/alpha[i]*(pow(lambdabar[0],alpha[i])+pow(lambdabar[1],alpha[i])+pow(lambdabar[2],alpha[i])-3);
      }
      return W+U;
#else
      return Scalar(0);
#endif
    }

};

} // namespace Simo

#endif
