#ifndef _RIGIDTETRAHEDRAL_H_
#define _RIGIDTETRAHEDRAL_H_

#include <Element.d/SuperElement.h>

class RigidTetrahedral : public SuperElement
{
public:
	explicit RigidTetrahedral(int*);
	int getElementType() const override { return 150; }
	int getTopNumber() const override { return 123; }
	bool isRigidElement() const override { return true; }
	bool isSafe() const override { return true; }
	PrioInfo examine(int sub, MultiFront*) override;

	FullSquareMatrix massMatrix(const CoordSet& cs, double *mel, int cmflg=1) const override;
	double getMass(const CoordSet& cs) const override;
	void getGravityForce(CoordSet&, double *gravity, Vector&, int gravflg,
						 GeomState *gs) override;
};

#endif

