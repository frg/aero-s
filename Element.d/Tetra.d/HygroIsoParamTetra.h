#ifndef _HYGROISOPARAMTETRA_H_
#define _HYGROISOPARAMTETRA_H_

#include <Element.d/Element.h>

class HygroIsoParamTetra: public Element {
public:
	int order;
	int *nn;
	HygroIsoParamTetra(const HygroIsoParamTetra& e);

public:
	HygroIsoParamTetra(int,int*);

	int getElementType() const override { return 55; }
	FullSquareMatrix stiffness(const CoordSet&, double *d, int flg=1) const override;
	FullSquareMatrix massMatrix(const CoordSet&,double *d, int cmflg=1) const override;
	double  getMass(const CoordSet& cs) const override;

	Category getCategory() const override { return Category::Hygro; }
	Element *clone() override;
	void renum(const int *) override;
	void renum(EleRenumMap&) override;
	void markDofs(DofSetArray &) const override;
	int* dofs(DofSetArray &, int *p) const override;
	int numDofs() const override { return (order*(order+1)*(order+2))/6; }
	int numNodes() const override;
	int* nodes(int * = 0) const override;

	PrioInfo examine(int sub, MultiFront *mf) override;
	int getTopNumber() const override;

	Corotator * getCorotator(CoordSet &, double*, int, int) override;
};
#endif
