#ifndef _QUAD8SOMMERBC_H_
#define _QUAD8SOMMERBC_H_ 

#include <Element.d/Sommerfeld.d/SommerElement.h>
#include <Utils.d/MyComplex.h>

class Quad8SommerBC: public SommerElement {

	int nn[8];
	double sommer_const;

public:

	Quad8SommerBC(int, int, int, int, int, int, int, int, double);

        int getElementType() const override { return 5; }
        int getNode(int nd) const override { return nn[nd]; }
        int numNodes() const override {return 8;}

        void renum(int *);

        FullSquareMatrix sommerMatrix(CoordSet&);
        FullSquareMatrix sommerMatrix(CoordSet&, double *) const override;

        FullSquareMatrix interfMatrixConsistent(CoordSet&);
        FullSquareMatrix interfMatrixConsistent(CoordSet&, double *);

        FullSquareMatrix interfMatrixLumped(CoordSet&);
        FullSquareMatrix interfMatrixLumped(CoordSet&, double *);

        int* dofs(DofSetArray &, int *p=0) const override;
        int  numDofs() const override;

        int* nodes(int * = 0);

        const int *getNodes() const override { return nn; }
        int* getNodes() override { return nn; }

        void neumVector(CoordSet &, DComplex*, double, double, double, double);

        void ffpNeum(int, DComplex*, CoordSet&, DComplex*, double,
                     double(*)[3], double*);
        void ffpDir(int, DComplex*, CoordSet&, DComplex*, double,
                    double(*)[3], double*);

	void getNormal(const CoordSet &, double[3]) const override;

};

#endif

