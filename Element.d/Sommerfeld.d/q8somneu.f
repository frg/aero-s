C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C     The calling sequence is
C
C       CALL   Q8SOMNEU ( X, Y, Z, P, DIR, NV)
C
C     where the input arguments are
C
C       X         (8 x 1) array of x coordinates of quadrilateral nodes
C       Y         (8 x 1) array of y coordinates of quadrilateral nodes
C       Z         (8 x 1) array of z coordinates of quadrilateral nodes
C       P         Gauss quadrature rule (no. of points)
C       DIR       (3x1) array of wave direction
C
C     The outputs are:
C
C       NV        (8 x 1) computed element Neumann vector
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      subroutine    q8somneu(x, y, z, p, dir, nv)
C
C                   A R G U M E N T S
C
      integer           p
      double precision  x(*), y(*), z(*)
      double precision  dir(*)
      complex*16        nv(*)
C
C                   L O C A L   V A R I A B L E S
C
      double precision  q(8), q1(8), q2(8)
      double precision  d1, d2, d3, d4, d13, d24
      double precision  d1h, d2h, d3h, d4h
      double precision  xd1, yd1, zd1, xd2, yd2, zd2
      double precision  xgp, ygp, zgp
      double precision  xi, eta, det, w, weight
      double precision  nux, nuy, nuz
      integer           j, k, l
      complex*16        ndotk, xdotk
C
C                   L O G I C
C
      do j = 1,8
          nv(j) = 0.0d0
      end do
C
      do k = 1,p
        do l = 1,p
C
          call     QGAUSS (p, k, p, l, xi, eta, weight)
C
          d1   = 0.5 * (1.0+xi)
          d2   = 0.5 * (1.0+eta)
          d3   = 1.0 - d1
          d4   = 1.0 - d2
          d1h  = 0.5 * d1
          d2h  = 0.5 * d2
          d3h  = 0.5 * d3
          d4h  = 0.5 * d4
          d13  = d1 * d3
          d24  = d2 * d4
C
          q(1) = d3*d4*(-xi-eta-1.0)
          q(2) = d4*d1*(+xi-eta-1.0)
          q(3) = d1*d2*(+xi+eta-1.0)
          q(4) = d2*d3*(-xi+eta-1.0)
          q(5) = 4.0*d1*d3*d4
          q(6) = 4.0*d2*d4*d1
          q(7) = 4.0*d3*d1*d2
          q(8) = 4.0*d4*d2*d3
C
          q1(1) = -d4h*(-xi-eta-1.0) - d3*d4
          q1(2) =  d4h*(+xi-eta-1.0) + d4*d1
          q1(3) =  d2h*(+xi+eta-1.0) + d1*d2
          q1(4) = -d2h*(-xi+eta-1.0) - d2*d3
          q1(5) = -2.0*xi*d4
          q1(6) =  2.0*d24
          q1(7) = -2.0*xi*d2
          q1(8) = -2.0*d24
C
          q2(1) = -d3h*(-xi-eta-1.0) - d3*d4
          q2(2) = -d1h*(+xi-eta-1.0) - d4*d1
          q2(3) =  d1h*(+xi+eta-1.0) + d1*d2
          q2(4) =  d3h*(-xi+eta-1.0) + d2*d3
          q2(5) = -2.0*d13
          q2(6) = -2.0*eta*d1
          q2(7) =  2.0*d13
          q2(8) = -2.0*eta*d3
C
          xd1 = 0.0 
          yd1 = 0.0 
          zd1 = 0.0 
          xd2 = 0.0 
          yd2 = 0.0 
          zd2 = 0.0 
C
          xgp = 0.0
          ygp = 0.0
          zgp = 0.0
C
          do j = 1,8
            xd1 = xd1 + x(j)*q1(j)
            yd1 = yd1 + y(j)*q1(j)
            zd1 = zd1 + z(j)*q1(j)
            xd2 = xd2 + x(j)*q2(j)
            yd2 = yd2 + y(j)*q2(j)
            zd2 = zd2 + z(j)*q2(j)
            xgp = xgp + x(j)*q(j)
            ygp = ygp + y(j)*q(j)
            zgp = zgp + z(j)*q(j)
          end do
C
          nux = yd1*zd2 - zd1*yd2
          nuy = zd1*xd2 - xd1*zd2
          nuz = xd1*yd2 - yd1*xd2
C
          det = nux**2 + nuy**2 + nuz**2
          det = SQRT(det)
C
          if (det .eq. 0.0)      then
            write(6,*) 'Zero Jacobian determinant'
            stop 
          end if
C
          nux = nux/det
          nuy = nuy/det
          nuz = nuz/det
C
          w = weight * det 
C
          ndotk = cmplx(0.0, nux*dir(1)+nuy*dir(2)+nuz*dir(3))
          xdotk = cmplx(0.0, xgp*dir(1)+ygp*dir(2)+zgp*dir(3))
C
          do j=1,8
            nv(j) = nv(j) + q(j) * w * ndotk * exp(xdotk)
          end do
C
        end do
      end do
C
      return
      end
