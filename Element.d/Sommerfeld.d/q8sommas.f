C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C     This routine forms the element consistent mass matrix of a
C     eight-node quadrilateral for 1 dof per node 
C
C     The calling sequence is
C
C       CALL   Q8SOMMAS ( X, Y, Z, P, MM)
C
C     where the input arguments are
C
C       X         (8 x 1) array of x coordinates of quadrilateral nodes
C       Y         (8 x 1) array of y coordinates of quadrilateral nodes
C       Z         (8 x 1) array of z coordinates of quadrilateral nodes
C       P         Gauss quadrature rule (no. of points)
C       M         First dimension of MM in calling program.
C
C     The outputs are:
C
C       MM        (8 x 8) computed element mass matrix.
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      subroutine    q8sommas(x, y, z, p, mm)
C
C                   A R G U M E N T S
C
      integer           p
      double precision  x(*), y(*), z(*)
      double precision  mm(8,*)
C
C                   L O C A L   V A R I A B L E S
C
      double precision  q(8), q1(8), q2(8)
      double precision  d1, d2, d3, d4, d13, d24
      double precision  d1h, d2h, d3h, d4h
      double precision  xd1, yd1, zd1, xd2, yd2, zd2
      double precision  xi, eta, det, w, weight, c1
      integer           i, j, k, l
C
C                   L O G I C
C
      do 1200  j = 1,8
        do 1100  i = 1,8
          mm(i,j) = 0.0
 1100     continue
 1200   continue

C
      do 3000  k = 1,p
        do 2500  l = 1,p
C
          call     QGAUSS (p, k, p, l, xi, eta, weight)
C
          d1   = 0.5 * (1.0+xi)
          d2   = 0.5 * (1.0+eta)
          d3   = 1.0 - d1
          d4   = 1.0 - d2
          d1h  = 0.5 * d1
          d2h  = 0.5 * d2
          d3h  = 0.5 * d3
          d4h  = 0.5 * d4
          d13  = d1 * d3
          d24  = d2 * d4
C
          q(1) = d3*d4*(-xi-eta-1.0)
          q(2) = d4*d1*(+xi-eta-1.0)
          q(3) = d1*d2*(+xi+eta-1.0)
          q(4) = d2*d3*(-xi+eta-1.0)
          q(5) = 4.0*d1*d3*d4
          q(6) = 4.0*d2*d4*d1
          q(7) = 4.0*d3*d1*d2
          q(8) = 4.0*d4*d2*d3
C
          q1(1) = -d4h*(-xi-eta-1.0) - d3*d4
          q1(2) =  d4h*(+xi-eta-1.0) + d4*d1
          q1(3) =  d2h*(+xi+eta-1.0) + d1*d2
          q1(4) = -d2h*(-xi+eta-1.0) - d2*d3
          q1(5) = -2.0*xi*d4
          q1(6) =  2.0*d24
          q1(7) = -2.0*xi*d2
          q1(8) = -2.0*d24
C
          q2(1) = -d3h*(-xi-eta-1.0) - d3*d4
          q2(2) = -d1h*(+xi-eta-1.0) - d4*d1
          q2(3) =  d1h*(+xi+eta-1.0) + d1*d2
          q2(4) =  d3h*(-xi+eta-1.0) + d2*d3
          q2(5) = -2.0*d13
          q2(6) = -2.0*eta*d1
          q2(7) =  2.0*d13
          q2(8) = -2.0*eta*d3
C
          xd1 = 0.0 
          yd1 = 0.0 
          zd1 = 0.0 
          xd2 = 0.0 
          yd2 = 0.0 
          zd2 = 0.0 
C
          DO i = 1,8
            xd1 = xd1 + x(i)*q1(i)
            yd1 = yd1 + y(i)*q1(i)
            zd1 = zd1 + z(i)*q1(i)
            xd2 = xd2 + x(i)*q2(i)
            yd2 = yd2 + y(i)*q2(i)
            zd2 = zd2 + z(i)*q2(i)
          END DO
C
          det = (xd1*yd2 - yd1*xd2)**2
          det = det + (yd1*zd2 - zd1*yd2)**2
          det = det + (zd1*xd2 - xd1*zd2)**2
          det = SQRT(det)
C
          if (det .eq. 0.0)      then
            write(6,*) 'Zero Jacobian determinant'
            stop 
          end if
C
          w = weight * det 
C
          do 2000  j = 1,8
            c1 =    q(j) * w
            do 1500  i = j,8
              mm(i,j) =  mm(i,j) + q(i)*c1 
              mm(j,i) =  mm(i,j)
 1500         continue
 2000       continue
 2500     continue
 3000   continue
 
C
      return
      end
