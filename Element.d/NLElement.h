#ifndef _NLELEMENT_H_
#define _NLELEMENT_H_
#include <Element.d/Element.h>
#include <Element.d/ExternalState.h>
#include <Math.d/FullSquareMatrix.h>
#include <Math.d/Vector.h>
#include <cstdio>

// Declaration of the Material Non linear element
class MatNLElement : public Element {
  double RotMat[36] = {1,0,0,0,0,0 , 0,1,0,0,0,0 , 0,0,1,0,0,0 , 0,0,0,1,0,0 , 0,0,0,0,1,0 , 0,0,0,0,0,1}; //Identity matrix
  double T33[9] = {1,0,0, 0,1,0, 0,0,1}; //Identity matrix
public:
    MatNLElement() {}

    Category getCategory() const override { return Category::Structural; }

    virtual void getStiffAndForce(Node *nodes, double *disp,
                                  double *state, FullSquareMatrix &kTan,
                                  double *force) {
        fprintf(stderr, "MatNLElement::getStiffAndForce is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void integrate(Node *nodes, double *dispn, double *staten,
                           double *dispnp, double *statenp,
                           FullSquareMatrix &kTan,
                           double *force, double dt,
                           std::vector<ExternalStateHandler::State<double>> &externalStates) {
        int nst = numStates();
        int i;
        for(i = 0; i < nst; ++i)
            statenp[i] = staten[i];
        updateStates(nodes, dispn, dispnp, statenp, externalStates);
        getStiffAndForce(nodes, dispnp, statenp, kTan, force);
    }
    virtual void getInternalForce(Node *nodes, double *disp,
                                  double *state, double *force) {
        fprintf(stderr, "MatNLElement::getInternalForce is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void integrate(Node *nodes, double *dispn, double *staten,
                           double *dispnp, double *statenp,
                           double *force, double dt,
                           std::vector<ExternalStateHandler::State<double>> &externalStates) {
        int nst = numStates();
        int i;
        for(i = 0; i < nst; ++i)
            statenp[i] = staten[i];
        updateStates(nodes, dispn, dispnp, statenp, externalStates);
        getInternalForce(nodes, dispnp, statenp, force);
    }
    virtual void integrateBlock(std::vector<MatNLElement *>& elems, std::vector<Node *>& nodesBlock,
                                std::vector<double *>& dispnBlock, std::vector<double *>& statenBlock,
                                std::vector<double *>& dispnpBlock, std::vector<double *>& statenpBlock,
                                FullSquareMatrix *kTanArray, std::vector<Vector>& forceBlock, double dt,
                                std::vector<std::vector<ExternalStateHandler::State<double>>>& externalStatesBlock,
                                std::vector<int>& blockElems) {
      for(int i = 0; i < elems.size(); ++i) {
        elems[i]->integrate(nodesBlock[i], dispnBlock[i], statenBlock[i], dispnpBlock[i], statenpBlock[i],
                            kTanArray[blockElems[i]], forceBlock[i].data(), dt, externalStatesBlock[i]);
      }
    }
    virtual void integrateBlock(std::vector<MatNLElement *>& elems, std::vector<Node *>& nodesBlock,
                                std::vector<double *>& dispnBlock, std::vector<double *>& statenBlock,
                                std::vector<double *>& dispnpBlock, std::vector<double *>& statenpBlock,
                                std::vector<Vector>& forceBlock, double dt,
                                std::vector<std::vector<ExternalStateHandler::State<double>>>& externalStatesBlock) {
      for(int i = 0; i < elems.size(); ++i) {
        elems[i]->integrate(nodesBlock[i], dispnBlock[i], statenBlock[i], dispnpBlock[i], statenpBlock[i],
                            forceBlock[i].data(), dt, externalStatesBlock[i]);
      }
    }
    virtual void setRotMat(const double _T33[9], const double _RotMat[36]) {
      for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
          T33[3*i+j] = _T33[3*i+j];

      for(int i=0; i<6; i++)
        for(int j=0; j<6; j++)
          RotMat[6*i+j] = _RotMat[6*i+j];
    }
    virtual void setRotMat(double _RotMat[36]) {
        for(int i=0; i<6; i++){
          for(int j=0; j<6; j++){
            RotMat[6*i+j] = _RotMat[6*i+j];
          }
        }
    }
    virtual void getRotMat33(double _T33[9]) const {
      for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
          _T33[3*i+j] = T33[3*i+j];
    }
    virtual void getRotMat66(double _RotMat[36]) const {
        for(int i=0; i<6; i++){
          for(int j=0; j<6; j++){
            _RotMat[6*i+j] = RotMat[6*i+j];
          }
        }
    }
    virtual void updateStates(Node *nodes, double *un, double *unp, double *statenp,
                              std::vector<ExternalStateHandler::State<double>> &externalStates, double dt=0) {
        fprintf(stderr, "MatNLElement::updateStates is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getStrainTens(Node *nodes, double *dispnp, double (*result)[9], int avgnum) {
        fprintf(stderr, "MatNLElement::getStrainTens is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getVonMisesStrain(Node *nodes, double *dispnp, double *result, int avgnum) {
        fprintf(stderr, "MatNLElement::getVonMisesStrain is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getStressTens(Node *nodes, double *dispn, double *staten,
                               double *dispnp, double *statenp, double (*result)[9], int avgnum,
                               std::vector<ExternalStateHandler::State<double>> &externalStates) {
        fprintf(stderr, "MatNLElement::getStressTens is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getVonMisesStress(Node *nodes, double *dispn, double *staten,
                                   double *dispnp, double *statenp, double *result, int avgnum,
                                   std::vector<ExternalStateHandler::State<double>> &externalStates) {
        fprintf(stderr, "MatNLElement::getVonMisesStress is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getEquivPlasticStrain(double *statenp, double *result, int avgnum) {
        fprintf(stderr, "MatNLElement::getEquivPlasticStrain is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getBackStressTens(double *statenp, double (*result)[9], int avgnum) {
        fprintf(stderr, "MatNLElement::getBackStressTens is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getPlasticStrainTens(double *statenp, double (*result)[9], int avgnum) {
        fprintf(stderr, "MatNLElement::getPlasticStrainTens is being called on an element "
                        "for which it is not defined\n");
    }
    virtual void getDamage(double *statenp, double *result, int avgnum) {
        fprintf(stderr, "MatNLElement::getDamage is being called on an element "
                        "for which it is not defined\n");
    }

    virtual double getStrainEnergy(Node *nodes, double *dispnp, double *state,
                                   std::vector<ExternalStateHandler::State<double>> &externalStates) {
        fprintf(stderr, "MatNLElement::getStrainEnergy is being called on an element "
                        "for which it is not defined\n");
        return 0.0;
    }

    virtual double getDissipatedEnergy(Node *nodes, double *state) {
        fprintf(stderr, "MatNLElement::getDissipatedEnergy is being called on an element "
                        "for which it is not defined\n");
        return 0.0;
    }

    virtual int getNumGaussPoints() { return 0; }

    virtual bool checkFailure(double *statenp) { return false; }
};
#endif
