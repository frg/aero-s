#ifndef _TETRAP1P0_H_
#define _TETRAP1P0_H_

#if ((__cplusplus >= 201103L) || defined(HACK_INTEL_COMPILER_ITS_CPP11))
#include <Element.d/Function.d/StrainEnergy.d/ThreeFieldStrainEnergyFunction.h>
#include <Element.d/Function.d/Shape.d/Tet4LagrangePolynomial.h>
#include <Element.d/Function.d/Shape.d/Constant.h>
#include <Element.d/Function.d/QuadratureRule.h>
#include <Element.d/Force.d/MixedFiniteElement.h>

template <typename S>
using TetraP1P0ThreeFieldStrainEnergyFunction 
      = Simo::ThreeFieldStrainEnergyFunction<S, Tet4LagrangePolynomialShapeFunction,
                                             ConstantShapeFunction, ConstantShapeFunction,
                                             TetrahedralQuadratureRule<double,Eigen::Vector3d> >;

class TetraP1P0 : public MixedFiniteElement<TetraP1P0ThreeFieldStrainEnergyFunction>
{
  public:
    static const DofSet NODALDOFS[4];
    TetraP1P0(int* _nn);

    int getElementType() const override { return 183; }
    Category getCategory() const override { return Category::Structural; }

    int getTopNumber() const override;
    PrioInfo examine(int sub, MultiFront *mf) override;
    int getQuadratureOrder() const override;

    int nDecFaces() const override { return 4; }
    int getDecFace(int iFace, int *fn) override;
    int getFace(int iFace, int *fn) override;
};
#endif
#endif
