#ifndef _INCOMPRESSIBLETETRAP1P0_H_
#define _INCOMPRESSIBLETETRAP1P0_H_

#if ((__cplusplus >= 201103L) || defined(HACK_INTEL_COMPILER_ITS_CPP11))
#include <Element.d/Function.d/StrainEnergy.d/FourFieldStrainEnergyFunction.h>
#include <Element.d/Function.d/Shape.d/Tet4LagrangePolynomial.h>
#include <Element.d/Function.d/Shape.d/Constant.h>
#include <Element.d/Function.d/QuadratureRule.h>
#include <Element.d/Force.d/MixedFiniteElement.h>

template <typename S>
using TetraP1P0FourFieldStrainEnergyFunction 
      = Simo::FourFieldStrainEnergyFunction<S, Tet4LagrangePolynomialShapeFunction,
                                            ConstantShapeFunction, ConstantShapeFunction, ConstantShapeFunction,
                                            TetrahedralQuadratureRule<double,Eigen::Vector3d> >;

class IncompressibleTetraP1P0 : public MixedFiniteElement<TetraP1P0FourFieldStrainEnergyFunction>
{
  public:
    static const DofSet NODALDOFS[4];
    IncompressibleTetraP1P0(int* _nn);

    int getElementType() const override { return 281; }
    Category getCategory() const override { return Category::Structural; }

    int getTopNumber() const override;
    PrioInfo examine(int sub, MultiFront *mf) override;
    int getQuadratureOrder() const override;

    int nDecFaces() const override { return 4; }
    int getDecFace(int iFace, int *fn) override;
    int getFace(int iFace, int *fn) override;
};
#endif
#endif
