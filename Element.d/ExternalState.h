//
// Created by Goéric Daeninck on 7/6/23.
//

#ifndef EXTERNALSTATE_H
#define EXTERNALSTATE_H

#include <cassert>
#include <array>
#include <vector>

/**
 * Structure for handling external state variables and pass this data without having to update function
 * signatures. Currently only temperature is supported, but other external state variables (such as moisture)
 * could be added in the future.
 *
 * If a new variable is added:
 *  - adjust "size" accordingly
 *  - implement the set / get functions for new variables
 *  - update getReferenceExternalState function in Element.C
 *  - implement dependencies on new variables inside material functions (search for
 *    ExternalStateHandler::getTemperature / setTemperature as a starting point)
 * */
struct ExternalStateHandler {

  static constexpr int temperatureIndex = 0;
  static constexpr int size = 1;

  template<typename Scalar>
  using State = std::array<Scalar, size>;

  static int numStates() {
    return size;
  }

  template<typename Scalar>
  static void setZeroState(Scalar *externalState) {
    std::fill(externalState, externalState+size, 0);
  }

  template<typename Scalar>
  static Scalar *getData(std::vector<State<Scalar>> &externalStates) {
    return &externalStates[0][0];
  }

  template<typename Scalar>
  static void set(int i, const Scalar &value, Scalar *externalState) {
    externalState[i] = value;
  }

  template<typename Scalar>
  static const Scalar &get(int i, const Scalar *externalState) {
    assert(i<size);
    return externalState[i];
  }

  template<typename Scalar>
  static Scalar &get(int i, Scalar *externalState) {
    assert(i<size);
    return externalState[i];
  }

  template<typename Scalar>
  static void setTemperature(const Scalar &temperature, Scalar *externalState) {
    if (temperatureIndex >= 0)
      externalState[temperatureIndex] = temperature;
  }

  template<typename Scalar>
  static const Scalar &getTemperature(const Scalar *externalState) {
    if (temperatureIndex >= 0)
      return externalState[temperatureIndex];
    else {
      static Scalar zero(0);
      return zero;
    }
  }

};

#endif //EXTERNALSTATE_H
