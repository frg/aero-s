***************************************************************
* THIS SUBROUTINE COMPUTE THE ELEMENT MASS MATRIX FOR THE 20- *
* NODE BRICK IN A CONSISTENT FORM.                            *
*                                                             *
***************************************************************
*                                                             *
*		VARIABLES 				      *
*                                                             *
*	     P = PxPxP GAUSS QUADRATURE RULE                  *
*	ELMASS = THE ELEMENT MASS MATRIX                      *
*	MXNSEQ = LEADING DIMENSION OF ELMASS                  *
*	     X = X COORDINATE ARRAY                           *
*	     Y = Y COORDINATE ARRAY                           *
*	     Z = Z COORDINATE ARRAY                           *
*                                                             *
***************************************************************
	subroutine helmbr20mas(p , elmass, x, y, z)
C
C..... DECLARE GLOBAL VARIABLES
C
C.... INTEGER CONSTANTS
C        implicit none
C
	integer p
C
C.... REAL ARRAYS
C
	real*8 elmass(20,20),z(*),x(*),y(*)
C
C.... DECLARE LOCAL VARIABLES FOR Q4DMAS
C
	integer k,l,jj
	real*8 c1,xi,eta,mu,weight,det,w
	real*8 q(20),qx(20),qy(20),qz(20)
C
	do 40 k = 1, 20
          do 50 l = 1, 20
            elmass(k,l) = 0.0d0
50	  continue
40	continue
C
C.... COMPUTE THE VOLUME OF THE BRICK, Element mass matrix 
C
	v = 0.0d0
	do 10 k=1, p
          do 20 l=1, p
            do 30 jj = 1, p
C
              call hxgaus20(p,k,p,l,p,jj,xi,eta,mu,weight)
              call h20shpe(xi,eta,mu,x,y,z,q,qx,qy,qz,det)
C
              if (det .le. 0.0)        then
                write(6,*)  'Negative Jacobian determinant'
                if (det .eq. 0.0)      then
                  write(6,*) 'Zero Jacobian determinant'
                end if
                stop
              end if
C
	      w = weight * det
C
            do 60  j = 1,20
              c1 = q(j) * w
              do 70  i = j,20
                elmass(i,j) = elmass(i,j) + q(i)*c1
                elmass(j,i) = elmass(i,j)
70            continue
60          continue
C
30	    continue
20	  continue
10	continue
C
	return
	end
