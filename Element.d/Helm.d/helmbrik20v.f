C=PURPOSE Form stiffness of 20-node iso-P hexahedron
C
C     The input arguments are
C
C       X         (20 x 1) array of x coordinates of hexahedron nodes
C       Y         (20 x 1) array of y coordinates of hexahedron nodes
C       Z         (20 x 1) array of z coordinates of hexahedron nodes
C       P         Gauss quadrature rule (no. of points in each dir)  p=3
C
C     The outputs are:
C
C       SM        (20 x 20) computed element stiffness matrix. 
C
C       STATUS    if error was detected.
C-----------------------------------------------------------------------------
      subroutine  helmbrik20v(x, y, z, p, sm, status)
C
C                   A R G U M E N T S
C
      integer           p,status
      double precision  x(20), y(20), z(20)
      double precision  sm(20,20)
C
C                   L O C A L   V A R I A B L E S
C
      double precision  q(20), qx(20), qy(20),qz(20)
      double precision  xi, eta, mu, det, w, weight
      double precision  cx, cy, cz
      integer           i, ix, iy, iz, j, jx, jy, jz, k, l, m
C
C                   L O G I C
C
      status =  0
      do 1200  j = 1,20
        do 1100  i = 1,20
          sm(i,j) = 0.0d0
 1100     continue
 1200   continue
C
      do 3000  k = 1,p
        do 2500  l = 1,p
          do 2400  m = 1,p
            call hxgaus20 (p, k, p, l, p, m, xi, eta, mu, weight)
            call h20shpe (xi, eta, mu, x, y, z, q, qx, qy, qz, det)
            if (det .le. 0.0d0) then
               write(6,*) 'Negative Jacobian determinant in helmbrik20v'
               status = -1
               return
            end if
C
            w =    weight * det 
C
            do 2000  j = 1,20
C
              cx = qx(j)*w
              cy = qy(j)*w
              cz = qz(j)*w
C
              do 1500  i = j,20
C
                sm(i,j) =sm(i,j) + qx(i)*cx + qy(i)*cy + qz(i)*cz
                sm(j,i) =sm(i,j)
C
 1500           continue
 2000         continue
 2400       continue
 2500     continue
 3000   continue
C
      return
      end
