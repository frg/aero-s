#ifndef _NLBAR_H_
#define _NLBAR_H_
#include <Utils.d/NodeSpaceArray.h>
#include <Element.d/NonLinearity.d/GaussIntgElem.h>
#include <Element.d/NonLinearity.d/ShapeFunction.h>
#include <Element.d/NonLinearity.d/NLMaterial.h>
#include <Element.d/NonLinearity.d/StrainEvaluator.h>
#include <Math.d/TTensor.h>

template <int n>
struct OneDTensorTypes
{
	static const int ndofs=n;
	typedef Stress1D StressTensor;
	typedef Stress1D StrainTensor;
	typedef SimpleTensor<StrainTensor, n> BTensor;
	typedef SimpleTensor<SimpleTensor<double,3>,1> GradUTensor;
	typedef SimpleTensor<GradUTensor, n> GradUDerivTensor;
	typedef SimpleTensor<BTensor,n> DBTensor;
	// Material tangent matrix tensor type
	typedef SymTensor<StressTensor,1> DTensor;
};

// Double contraction operator
template <int n>
SimpleTensor<Stress1D, n>
operator||(SimpleTensor<SimpleTensor<Stress1D,n>,n> &, SimpleTensor<Stress1D, n> &);

template <int n>
class LinearStrain1D : public GenStrainEvaluator<OneDTensorTypes<n> >
{
	void getE(typename OneDTensorTypes<n>::StrainTensor &e,
			  typename OneDTensorTypes<n>::GradUTensor &gradU);
	void getEandB(typename OneDTensorTypes<n>::StrainTensor &e,
				  typename OneDTensorTypes<n>::BTensor &B,
				  typename OneDTensorTypes<n>::GradUTensor &gradU,
				  typename OneDTensorTypes<n>::GradUDerivTensor &dgradUdqk);
	void getEBandDB(typename OneDTensorTypes<n>::StrainTensor &e,
					typename OneDTensorTypes<n>::BTensor &B,
					typename OneDTensorTypes<n>::DBTensor &DB,
					typename OneDTensorTypes<n>::GradUTensor &gradU,
					typename OneDTensorTypes<n>::GradUDerivTensor &dgradUdqk);
};

template <int n>
class GLStrain1D : public GenStrainEvaluator<OneDTensorTypes<n> >
{
public:
	void getE(typename OneDTensorTypes<n>::StrainTensor &e,
			  typename OneDTensorTypes<n>::GradUTensor &gradU);
	void getEandB(typename OneDTensorTypes<n>::StrainTensor &e,
				  typename OneDTensorTypes<n>::BTensor &B,
				  typename OneDTensorTypes<n>::GradUTensor &gradU,
				  typename OneDTensorTypes<n>::GradUDerivTensor &dgradUdqk);
	void getEBandDB(typename OneDTensorTypes<n>::StrainTensor &e,
					typename OneDTensorTypes<n>::BTensor &B,
					typename OneDTensorTypes<n>::DBTensor &DB,
					typename OneDTensorTypes<n>::GradUTensor &gradU,
					typename OneDTensorTypes<n>::GradUDerivTensor &dgradUdqk);
	bool isNonLinear() { return true; }
};

class BarShapeFunct : public GenShapeFunction< OneDTensorTypes<6> >
{
public:
	BarShapeFunct() : GenShapeFunction< OneDTensorTypes<6> >() {}
	void getGlobalGrads(Grad1D &gradU, Grad1DDeriv6 &dGradUdqk, double *_jac,
						Node *nodes, double xi[3], Vector &disp) override;
	void getGradU(Grad1D &gradU,
				  Node *nodes, double xi[3], Vector &disp) override;
	double interpolateScalar(double *_q, double _xi[3]) override;
	void interpolateScalarData(double *interpolatedData, double *nodalData, int dataSize, double _xi[3]) override;
};

class NLBar : public GenGaussIntgElement<OneDTensorTypes<6> >
{
	int n[2];
        NLMaterial  *material, *linearMaterial;
	bool useDefaultMaterial;
protected:
	int getNumGaussPoints() const override;
	void getGaussPointAndWeight(int i, double *point, double &weight) const override;
    void getLocalNodalCoords(int, double *) override;
	GenShapeFunction< OneDTensorTypes<6> > *getShapeFunction() const override;
	GenStrainEvaluator<OneDTensorTypes<6> > *getGenStrainEvaluator() const override;
	const NLMaterial  *getMaterial() const override;
    NLMaterial *getLinearMaterial() const override;
    GenStrainEvaluator<OneDTensorTypes<6> > *getLinearMaterialGenStrainEvaluator() const override;
    void rotateCFrame(const CoordSet &cs, double *T, double *Tinv) const;

public:
	explicit NLBar(int *nd);
	~NLBar() override;

	int getElementType() const override { return 191; }
	void setPreLoad(std::vector<double> &_preload) override { preload = _preload; }
	std::vector<double> getPreLoad() override { return preload; }
	int numNodes() const override { return 2; }
	int numDofs() const override { return 6; }
	void renum(const int *) override;
	void renum(EleRenumMap&) override;
	void markDofs(DofSetArray &) const override;
	int* dofs(DofSetArray &, int *p) const override;
	int* nodes(int *) const override;
	void updateStates(Node *nodes, double *states, double *un, double *unp) {}
	void setProp(StructProp *p, bool _myProp) override;
	void setMaterial(NLMaterial *) override;

	Corotator* getCorotator(CoordSet &, double *, int , int) override;
	int getTopNumber() const override { return 101; }
	FullSquareMatrix  stiffness(const CoordSet& cs, double *k, int flg) const override;
        int getMassType() const override { return 2; } // both consistent and lumped
	
        FullSquareMatrix massMatrix(const CoordSet& cs, double *mel, int cmflg) const override;
	double getMass(const CoordSet&) const override;

        void getGravityForce(CoordSet& cs, double *gravityAcceleration, Vector &force, int, GeomState * = 0) override;

        void buildFrame(CoordSet &cs) override;

	void computeDisp(CoordSet &cs, State &state, const InterpPoint &, double *res, GeomState *gs) override;

        void getFlLoad(CoordSet &cs, const InterpPoint &ip, double *flF, double *res, GeomState *gs) override;

	PrioInfo examine(int sub, MultiFront *mf) override;
};
#endif
