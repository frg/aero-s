#ifndef _1DMAT_H_
#define _1DMAT_H_

#include <Element.d/NonLinearity.d/NLMaterial.h>

class StructProp;
class MFTTData;
template <class S, int d> class SymTensor;

class ElaLinMat1D : public NLMaterial
{
   protected:
     // isotropic material properties
     double rho, E, A, alpha;
     // reference temperature
     double Tref;
     // temperature dependent material properties
     MFTTData *ymtt, *prtt, *ctett;

   public:
     ElaLinMat1D(StructProp *p);
     ElaLinMat1D(double _rho, double _E, double _A, double _Tref, double _alpha);
     virtual ~ElaLinMat1D();

     int getNumStates() const override { return 0; }

     void getStress(Tensor *stress, Tensor &strain, double*, double *externalState) override;

     void getTangentMaterial(Tensor *tm, Tensor &strain, double*, double *externalState) override;

     void getElasticity(Tensor *tm) const override {};

     void updateStates(Tensor &en, Tensor &enp, double *state, double *externalState) override {};

     void getStressAndTangentMaterial(Tensor *stress, Tensor *tm, Tensor &strain, double*, double *externalState) override;
     
     void integrate(Tensor *stress, Tensor *tm, Tensor &en, Tensor &enp,
                    double *staten, double *statenp, double *externalState,
                    Tensor *cache, double dt=0) const override;

     void integrate(Tensor *stress, Tensor &en, Tensor &enp,
                    double *staten, double *statenp, double *externalState,
                    Tensor *cache, double dt=0) const override;

     void initStates(double *) override {};

     GenStrainEvaluator<OneDTensorTypes<6> > * get1DStrainEvaluator() override;

     double getDensity() override { return rho; }

     double getArea() const override { return A; }

     double getReferenceTemperature() override { return Tref; }

    void setTDProps(MFTTData *_ymtt, MFTTData *_prtt, MFTTData *_ctett) override { ymtt = _ymtt, prtt = _prtt, ctett = _ctett; }
};

// same equation as ElaLinMat1D but with different Green-Lagrange strain evaluator
// (also known as St. Venant-Kirchhoff hyperelastic material
class StVenantKirchhoffMat1D : public ElaLinMat1D
{
  public:
    StVenantKirchhoffMat1D(StructProp *p) : ElaLinMat1D(p) {}
    StVenantKirchhoffMat1D(double rho, double E, double A, double Tref, double alpha) : ElaLinMat1D(rho, E, A, Tref, alpha) {}

    GenStrainEvaluator<OneDTensorTypes<6> > * get1DStrainEvaluator();
};


#endif
