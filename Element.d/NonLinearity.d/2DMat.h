#ifndef _2DMAT_H_
#define _2DMAT_H_

#include <Element.d/NonLinearity.d/NLMaterial.h>

class StructProp;
class MFTTData;
template <class S, int d> class SymTensor;

class ElaLinIsoMat2D : public NLMaterial
{
   protected:
     // isotropic material properties
     double rho, E, nu, t, alpha;
     // anisotropic material properties
     SymTensor<SymTensor<double,2>,2> *m_tm;
     double alphas[3];
     // reference temperature
     double Tref;
     // temperature dependent material properties
     MFTTData *ymtt, *prtt, *ctett;

   public:
     ElaLinIsoMat2D(StructProp *p);
     ElaLinIsoMat2D(double _rho, double _E, double _nu, double _t, double _Tref, double _alpha);
     virtual ~ElaLinIsoMat2D();

     int getNumStates() const override { return 0; }

     void getStress(Tensor *stress, Tensor &strain, double*, double *externalState) override;

     void getTangentMaterial(Tensor *tm, Tensor &strain, double*, double *externalState) override;

     void getElasticity(Tensor *tm) const override {};

     void updateStates(Tensor &en, Tensor &enp, double *state, double *externalState) override {};

     void getStressAndTangentMaterial(Tensor *stress, Tensor *tm, Tensor &strain, double*, double *externalState) override;
     
     void integrate(Tensor *stress, Tensor *tm, Tensor &en, Tensor &enp,
                    double *staten, double *statenp, double *externalState,
                    Tensor *cache, double dt=0) const override;

     void integrate(Tensor *stress, Tensor &en, Tensor &enp,
                    double *staten, double *statenp, double *externalState,
                    Tensor *cache, double dt=0) const override;

     void initStates(double *) override {};

     GenStrainEvaluator<TwoDTensorTypes<9> > * get2DStrainEvaluator() override;

     double getDensity() override { return rho; }

     double getThickness() const override { return t; }

     double getReferenceTemperature() override { return Tref; }

    void setTangentMaterial(double C[6][6]) override;

    void setThermalExpansionCoef(double alphas[6]) override;

    void setTDProps(MFTTData *_ymtt, MFTTData *_prtt, MFTTData *_ctett) override { ymtt = _ymtt, prtt = _prtt, ctett = _ctett; }
};

// same equation as ElaLinIsoMat2D but with different Green-Lagrange strain evaluator
// (also known as St. Venant-Kirchhoff hyperelastic material
class StVenantKirchhoffMat2D : public ElaLinIsoMat2D
{
  public:
    StVenantKirchhoffMat2D(StructProp *p) : ElaLinIsoMat2D(p) {}
    StVenantKirchhoffMat2D(double rho, double E, double nu, double t, double Tref, double alpha) : ElaLinIsoMat2D(rho, E, nu, t, Tref, alpha) {}

    GenStrainEvaluator<TwoDTensorTypes<9> > * get2DStrainEvaluator();
};


#endif
