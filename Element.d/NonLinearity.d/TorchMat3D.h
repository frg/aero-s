#ifndef _TORCHMAT3D_H_
#define _TORCHMAT3D_H_

#include <Element.d/NonLinearity.d/NLMaterial.h>

#include <array>
#include <limits>
#include <string>

class StructProp;

class TorchMat3D : public NLMaterial
{
public:
	enum StrainMeasure { INFINTESIMAL = 0, GREEN_LAGRANGE = 1 };

protected:
	std::string fname;
	double sf; // scaling factor (to account for change in units of stress)
	// Neural network : eps -> sigma/sf instead of eps -> sigma
	double Tref, alpha;
	enum StrainMeasure strain_measure;
        int toVoigt[6] = {0, 5 ,4, 1, 3, 2};  // convert from aero-s indexing to voigt indexing
        int fromVoigt[6] = {0, 3, 5, 4 ,2 ,1};  // convert from voigt indexing to aero-s indexing

public:
	TorchMat3D(std::string _fname, double _sf, double _Tref, double _alpha,
                 StrainMeasure _strain_measure);

	int getNumStates() const override { return 0; }

	void getStress(Tensor *stress, Tensor &strain, double*, double *externalState) override;

	void getTangentMaterial(Tensor *tm, Tensor &strain, double*, double *externalState) override;

	void getElasticity(Tensor *tm) const override {};

	void updateStates(Tensor &en, Tensor &enp, double *state, double *externalState) override {};

	void getStressAndTangentMaterial(Tensor *stress, Tensor *tm, Tensor &strain, double*, double *externalState) override;

	void integrate(Tensor *stress, Tensor *tm, Tensor &en, Tensor &enp,
	               double *staten, double *statenp, double *externalState,
	               Tensor *cache, double dt=0) const override;

	void integrate(Tensor *stress, Tensor &en, Tensor &enp,
	               double *staten, double *statenp, double *externalState,
	               Tensor *cache, double dt=0) const override;

        NLMaterial * clone() const override;

        void integrateBlock(std::vector<Tensor *>& stress, std::vector<Tensor *>& tm, std::vector<Tensor *>& en, std::vector<Tensor *>& enp,
                            std::vector<double *>& staten, std::vector<double *>& statenp,
          std::vector<std::array<double,ExternalStateHandler::size>>& externalState,
                            std::vector<Tensor *> *cache, double dt=0) const override;

        void integrateBlock(std::vector<Tensor *>& stress, std::vector<Tensor *>& en, std::vector<Tensor *>& enp,
                            std::vector<double *>& staten, std::vector<double *>& statenp,
                            std::vector<std::array<double,ExternalStateHandler::size>>& externalState,
                            std::vector<Tensor *> *cache, double dt=0) const override;

	void initStates(double *) override {};

        StrainEvaluator * getStrainEvaluator() const override;

	double getDensity() override { return 0.; }

	double getReferenceTemperature() override { return Tref; }

private:
	void callTorch(double *strain, double *stress) const;
	void callTorch(double *strain, double *stress, double *tangents) const;
	void callTorchBlock(std::vector<std::array<double,6>>& input, std::vector<std::array<double,6>>& output) const;
	void callTorchBlock(std::vector<std::array<double,6>>& input, std::vector<std::array<double,6>>& output,
			    std::vector<std::array<double,36>>& tangents) const;
};

#endif
