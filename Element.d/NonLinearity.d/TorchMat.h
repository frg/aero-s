#ifndef _TORCHMAT_H_
#define _TORCHMAT_H_

#include <Element.d/NonLinearity.d/NLMaterial.h>

#include <array>
#include <limits>
#include <string>

class StructProp;

class TorchMat : public NLMaterial
{
public:
	enum StrainMeasure { INFINTESIMAL = 0, GREEN_LAGRANGE = 1 };

protected:
        mutable const std::string* fname;
	std::string fnameStress, fnameInternalStress, fnameInternalVarRate;
	double th; // thickness
	double sf; // scaling factor to NN output (to account for change in units of stress)
	double Tref, alpha;
	enum StrainMeasure strain_measure;
        bool elasticFlag;  //whether or not the stress is expressible as a function of only the current strain
        enum ModuleType {STRESS = 0, INTERNALSTRESS = 1, INTERNALVARRATE = 2};
public:
	TorchMat(std::string _fnameStress, std::string _fnameInternalStress, std::string _fnameInternalVarRate, double _th, double _sf, double _Tref, double _alpha,
                 StrainMeasure _strain_measure, bool _elasticFlag);

	int getNumStates() const override { return elasticFlag ? 0 : 6;} // for now only dealing with one strain-like IV and it's rate (alpha, alpha_dot)

	void getStress(Tensor *stress, Tensor &strain, double*, double *externalState) override;

	void getTangentMaterial(Tensor *tm, Tensor &strain, double*, double *externalState) override;

	void getElasticity(Tensor *tm) const override {};

	void updateStates(Tensor &en, Tensor &enp, double *state, double *externalState) override {};

	void getStressAndTangentMaterial(Tensor *stress, Tensor *tm, Tensor &strain, double*, double *externalState) override;

	void integrate(Tensor *stress, Tensor *tm, Tensor &en, Tensor &enp,
	               double *staten, double *statenp, double *externalState,
	               Tensor *cache, double dt=0) const override;

	void integrate(Tensor *stress, Tensor &en, Tensor &enp,
	               double *staten, double *statenp, double *externalState,
	               Tensor *cache, double dt=0) const override;

	void integrateBlock(std::vector<Tensor *>& stress, std::vector<Tensor *>& tm, std::vector<Tensor *>& en, std::vector<Tensor *>& enp,
			    std::vector<double *>& staten, std::vector<double *>& statenp,
          std::vector<std::array<double,ExternalStateHandler::size>>& externalState,
			    std::vector<Tensor *> *cache, double dt=0) const override;

	void integrateBlock(std::vector<Tensor *>& stress, std::vector<Tensor *>& en, std::vector<Tensor *>& enp,
                            std::vector<double *>& staten, std::vector<double *>& statenp,
                            std::vector<std::array<double,ExternalStateHandler::size>>& externalState,
                            std::vector<Tensor *> *cache, double dt=0) const override;

	void initStates(double *) override;

	GenStrainEvaluator<TwoDTensorTypes<9> > * get2DStrainEvaluator() override;

	double getDensity() override { return 0.; }

	double getThickness() const override { return th; }

	double getReferenceTemperature() override { return Tref; }

private:
	void callTorch(ModuleType module_type, double *input1, double *input2, double *output) const;
	void callTorch(ModuleType module_type, double *input1, double *input2, double *output, double *tangents) const;
	void callTorchBlock(ModuleType module_type, std::vector<std::array<double,3>>& input1, std::vector<std::array<double,3>>& input2, std::vector<std::array<double,3>>& output) const;
	void callTorchBlock(ModuleType module_type, std::vector<std::array<double,3>>& input1, std::vector<std::array<double,3>>& input2, std::vector<std::array<double,3>>& output, std::vector<std::array<double,9>>& tangents) const;
};

#endif
