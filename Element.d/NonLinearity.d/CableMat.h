#ifndef _CABLEMAT_H_
#define _CABLEMAT_H_

#include <Element.d/NonLinearity.d/NLMaterial.h>

#include <limits>
#include <string>

class StructProp;

class CableMat : public NLMaterial
{

// material law in the form of stress = E*A/cfa * log ( (e^cfb-1)*cfb*strain +1 ) -E*A*cfb/cfa
protected:
	double A; // cross sectional area
    double E;
    double cfa; //compressive factor a, controls sharpness
    double cfb; //compressive factor b, controls maximum compressive load
    double Tref, alpha;
public:
	CableMat(double _E, double _A, double _cfa, double _cfb, double _Tref, double _alpha);

        int getNumStates() const override { return 0; }

        void getStress(Tensor *stress, Tensor &strain, double*, double *externalState) override;

        void getTangentMaterial(Tensor *tm, Tensor &strain, double*, double *externalState) override;

        void getElasticity(Tensor *tm) const override {};

        void updateStates(Tensor &en, Tensor &enp, double *state, double *externalState) override {};

        void getStressAndTangentMaterial(Tensor *stress, Tensor *tm, Tensor &strain, double*, double *externalState) override;

        void integrate(Tensor *stress, Tensor *tm, Tensor &en, Tensor &enp,
                       double *staten, double *statenp, double *externalState,
                       Tensor *cache, double dt=0) const override;

        void integrate(Tensor *stress, Tensor &en, Tensor &enp,
                       double *staten, double *statenp, double *externalState,
                       Tensor *cache, double dt=0) const override;

	void initStates(double *) override {};

	GenStrainEvaluator<OneDTensorTypes<6> > * get1DStrainEvaluator() override;

	double getDensity() override { return 0.; }

	double getArea() const override { return A; }

	double getReferenceTemperature() override { return Tref; }

};

#endif
