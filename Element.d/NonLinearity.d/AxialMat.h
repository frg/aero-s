#ifndef _AXIALMAT_H_
#define _AXIALMAT_H_

#include <Element.d/NonLinearity.d/NLMaterial.h>

class StructProp;

template<class BaseMaterial>
class AxialMat : public BaseMaterial
{
     double A;

   public:
     AxialMat(double p1, double A);
     AxialMat(double p1, double p2, double A);
     AxialMat(double p1, double p2, double p3, double A);
     AxialMat(double p1, double p2, double p3, double p4, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10,
                    double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10, 
                    double p11, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10, 
                    double p11, double p12, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10,
                    double p11, double p12, double p13, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10,
                    double p11, double p12, double p13, double p14, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10,
                    double p11, double p12, double p13, double p14, double p15, double A);
     AxialMat(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10,
                    double p11, double p12, double p13, double p14, double p15, double p16, double A);

     int getNumStates() const override;

     void initStates(double *) override;

     void getStress(Tensor *stress, Tensor &strain, double*, double *externalState) override;

     void integrate(Tensor *stress, Tensor *tm, Tensor &en, Tensor &enp,
                    double *staten, double *statenp, double *externalState,
                    Tensor *cache, double dt=0) const override;

     void integrate(Tensor *stress, Tensor &en, Tensor &enp,
                    double *staten, double *statenp, double *externalStatetemp,
                    Tensor *cache, double dt=0) const override;

     void print(std::ostream &out) const override;

     NLMaterial * clone() const override;

     GenStrainEvaluator<OneDTensorTypes<6> > * get1DStrainEvaluator() override;

     double getArea() const override { return A; }
};

#ifdef _TEMPLATE_FIX_
  #include <Element.d/NonLinearity.d/AxialMat.C>
#endif

#endif
