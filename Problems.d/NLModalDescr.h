#ifndef _NL_MODAL_DESCR_H_
#define _NL_MODAL_DESCR_H_

#include <Problems.d/ModalBase.h>
#include <Problems.d/NLModalGeomState.h>
#include <Math.d/DBSparseMatrix.h>
#include <Math.d/matrix.h>

#include <iostream>

class NLModalOpSolver {
/* wrapper class for a FullMatrix
   passed to NLDynamSolver as template parameter OpSolver
*/
private:

  FullM mat;

public:

  NLModalOpSolver() {}
  void reSolve(Vector &rhs);

  friend class NLModalDescr;

};

//------------------------------------------------------------------------------
//******************************************************************************
//------------------------------------------------------------------------------

class NLModalDescr : public ModalBase {
/* problem descriptor for non-linear dynamics problem
   solves eq of motion in a body-fixed reference frame
*/

private:
  Vector fullFrot;

  Vector **modesPr;  // rotational modes of the deformed structure
  double mass;       // scaling factor equal to total mass of structure by default
  double J[3][3];    // mass moment of inertia tensor

  int numFilters;
  int *rbmIdx;         // indicies of the rbms to be filtered
  FullM *invPsiTMPsi;  // inverse of Psi^T.M.Psi used for the rbm filter
  FullM *MPsi;         // M.Psi; Psi is the matrix of rbms to be filtered

  double ****Bp;     // the various B quantities are coupling coefficients
  double  ***Bh;     // note to self: Bp is B'; Bh, Bhat; Bt, Btilde
  double  ***Bt;
  double  **Msu;     // part of reduced mass matrix coupling rigid translations and flexural modes

  double ***Bq;      // temporary variables to store intermediate products
  double ***Bo;
  double Bqq[3][3];
  double Bhq[3][3];
  double **Bho;
  double **Btq;
  double **Bqo;
  double **Bto;
  Vector q_modesPr[3];

  double *constr;       // the constraints evaluated at n+1 state

  double **dhdp;   // derivative of the constraints wrt generlaized translation
  double **dhdth;  //  "  "  "  wrt generalized rotation
  double **dhdq;   //  "  "  "  wrt modal deformation
  double ***d2h;   // 2nd derivative wrt modal deformation and rotation

  double **dhdp_np1;   // same derivatives as above, but evaluated
  double **dhdth_np1;  //   at the n+1  time step
  double **dhdq_np1;

  double c22a[3][3];    // temporary variables to store intermediate sums
  double c22b[3];       // c22b is used in the rhs and in NLModalOpSolver.mat
  double mcoef, ccoef;  // coefficients for the contribution of M, C and K in
  double kcoef, hcoef;  //   NLModalOpSolver.mat; hcoef is scaling factor for constraints

  NLModalOpSolver *solver;
  double tFinal;
  double dt;
  double delta;
  int    maxStep;
  double remainingTime;
  double tolerance;     // tolerance for convergence criteria
  double firstRes;      // norm of the residual of the first iteration of
                        //   a given time step
  ModalOps modalOps;
  Vector initDsp;

  GenSparseMatrix<double> *massMat;

public:

  NLModalDescr() {}
  NLModalDescr(Domain *d);

  void rotateVector(Vector& globalF, Vector& rotF, double glR[3][3], bool transpose = true);
  void rotateVectorInPlace(Vector& globalF, double glR[3][3], bool transpose = true);
  void projectForceNL(Vector& fullF, Vector& modalF, NLModalGeomState* mgs);
  void filterForce(Vector& fullF);
  void expandDsp(NLModalGeomState* mgs, Vector& fullDsp, bool excludeRM);
  void expandVel(NLModalGeomState* mgs, Vector& fullVel, bool excludeRM);
  void expandAcc(NLModalGeomState* mgs, Vector& fullAcc, bool excludeRM);
  void expandFlex(Vector& modalV, Vector& fullV);
  void calcTmps(NLModalGeomState &mgs);

  void preProcess();
  void processLastOutput();

  void buildRBMFilter(DBSparseMatrix* massMat);

  void computeTimeInfo();
  NLModalOpSolver* getSolver() { return solver; }

  int solVecInfo() { return (numRBM + numFlex + numConstr); }
  int sysVecInfo() { return domain->numdof(); }
  int elemVecInfo() { return domain->maxNumDOF(); }

  void getConstForce(Vector &constF);
  void getExternalForce(Vector &extF, Vector &gravF, int tIndex,
                        double time, NLModalGeomState* mgs, Vector &elemIntF, Vector& aeroF, double);
  void getInitAeroForce(Vector&, Vector&) { std::cerr << "NLModalDescr::getInitAeroForce is not implemented\n"; }

  int getInitState(Vector &dsp, Vector &vel, Vector &acc, Vector &vel_p);

  NLModalGeomState* createGeomState() { return new NLModalGeomState(numRBM, numFlex, numConstr, cg); }
  NLModalGeomState* copyGeomState(NLModalGeomState* mgs)
    { return new NLModalGeomState(*mgs); }

  void readRestartFile(Vector &dsp, Vector &vel, Vector &acc, Vector &vel_p,
                       NLModalGeomState &mgs);
  void updatePrescribedDisplacement(NLModalGeomState *mgs);
  
  int    getMaxit()   { return domain->solInfo().getNLInfo().maxiter; }
  int    getMaxStep() { return maxStep; }
  double getDt()      { return dt; }
  double getDelta()   { return delta; }

  void getInitialTime(int &initTimeIndex, double &initTime) {
    initTimeIndex = domain->solInfo().initialTimeIndex;
    initTime      = domain->solInfo().initialTime;
  }

  void initIterState(NLModalGeomState &mgs);
  double getStiffAndForce(NLModalGeomState &mgs, Vector &res, double midtime = -1);
  void reBuild(NLModalGeomState &mgs, int iter, double localDelta, double t);

  void evalRHS(Vector &res, Vector &rhs, NLModalGeomState &mgs);
  void formRHSinitializer(Vector &, Vector &, Vector &, NLModalGeomState &, Vector &, NLModalGeomState * = NULL);
  void formRHSpredictor(Vector &res, Vector &rhs, NLModalGeomState &mgs);
  double formRHScorrector(Vector& res, Vector& rhs, NLModalGeomState &mgs);
  void getIncDisplacement(NLModalGeomState *geomState, Vector &du, NLModalGeomState *refState, bool zeroRot) { geomState->get_inc_displacement(du, *refState); }

  int checkConvergence(int iter, double normRes, Vector &residual,
                       Vector &dvec, double time);

  double getTolerance() { return (tolerance*firstRes); }

  void dynamCommToFluid(NLModalGeomState* mgs, NLModalGeomState* bkMgs, 
                        Vector& vel, Vector& bkVel,
                        Vector& vel_p, Vector& bkVel_p,
                        int tIndex, int parity,
                        int aeroAlg, double time) {}
  void dynamOutput(NLModalGeomState* mgs, Vector& vel, Vector& vel_p,
                   double time, int tIndex, Vector& extF, Vector &aeroF, Vector &acc, NLModalGeomState*);
  void writeRestartFile(double time, int timeIndex, NLModalGeomState *mgs);
  void printTimers(double timeLoop) { /* leave blank for now */ }

  int getNumStages() { return 1; }

  void dRdTheta(double R[3][3], double dR[3][3][3]);
  void evalConstraintsAndDerivatives(NLModalGeomState &mgs);
  
  void initNewton() { /* do nothing */ }

  int getAeroAlg() { return domain->solInfo().aeroFlag; }
  int getThermoeFlag() { return domain->solInfo().thermoeFlag; }
  int getThermohFlag() { return domain->solInfo().thermohFlag; }
  int getAeroheatFlag() { return domain->solInfo().aeroheatFlag; }
  void getNewmarkParameters(double &beta, double &gamma,
                            double &alphaf, double &alpham);

  void getConstraintMultipliers(NLModalGeomState &geomState) {}
  double getResidualNorm(const Vector &rhs, NLModalGeomState &geomState, double localDelta);
  void initializeParameters(int step, NLModalGeomState *geomState) {}
  void updateParameters(NLModalGeomState *geomState) {}
  bool checkConstraintViolation(double &err, NLModalGeomState *geomState) { return false; }
  void updateContactSurfaces(NLModalGeomState& geomState, NLModalGeomState *refState) {}
  virtual void updateStates(NLModalGeomState *refState, NLModalGeomState& geomState, double time) {}

  LinesearchInfo& linesearch() { return domain->solInfo().getNLInfo().linesearch; }
  bool getResizeFlag() { return false; }
  void resize(NLModalGeomState *refState, NLModalGeomState *geomState, NLModalGeomState *stepState, Vector *stateIncr,
              Vector &v, Vector &a, Vector &vp, Vector &force) {}

  void preProcessSA() {}
  AllSensitivities<double> *getAllSensitivities() { return 0; }
  void sensitivityAnalysis(NLModalGeomState *, NLModalGeomState *) {}

  virtual void obtainKdyn(){} 

  #ifdef USE_EIGEN3 
    Eigen::MatrixXd Kdyn; 
    Eigen::MatrixXd redMass; 
    Eigen::MatrixXd derivativeTensor; 
    Eigen::MatrixXd da0dv; 
    void getResidualSensitivity(NLModalGeomState& geomState, Vector& residual, Vector& elementInternalForce,
                            double t, NLModalGeomState *refState, Vector& external_force){}
    void correctResidualSensitivity(NLModalGeomState& geomState, Vector& residual, Vector& elementInternalForce,
                            double t, NLModalGeomState *refState, Vector& external_force){}
    void getInitialResidualSensitivity(NLModalGeomState& geomState, Vector& residual, Vector& elementInternalForce,
                            double t, NLModalGeomState *refState, Vector& external_force, Eigen::MatrixXd& reducedMass){}
    #endif

    int getFullSize() const { return 0; }
};

#endif
