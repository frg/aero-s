#ifndef _NLMODAL_GEOM_STATE_H
#define _NLMODAL_GEOM_STATE_H

#include <Math.d/Vector.h>

#include <iostream>

class NLModalGeomState {
/* class used by NLModalDescr for storing the geometric state of the structure
   contains global translation; global rotation; and modal deformations
   passed as a tmeplate parameter GeomType to class NLDynamSolver defined in
   Driver.d/NLDynamProbType.[Ch] */
private:

  double glT[3];     // rigid body translation vector
  double glR[3][3];  // rigid body rotation tensor : global = glR*local
  Vector q;          // modal deformation coefficients
  Vector lam;        // Lagrange multipliers to enforce constraints if any

  Vector vel;
  Vector acc;

  int numRBM, numFlex, numConstr;

  // the state at time step n+1, used to evaluate constraints
  double glTnp1[3];
  double glRnp1[3][3];
  Vector qnp1;

  // center of gravity, used to update rigid modes' prescribed trajectory
  double cg[3];

public:

  NLModalGeomState(int nRMB, int nFlex, int nConstr, double _cg[3]);
  NLModalGeomState(const NLModalGeomState& mgs);

  void update(const Vector &dsp, double dton2);
  void get_inc_displacement(Vector &incDsp, NLModalGeomState &stepState)
    { /* NLModalDescr does not use incremental displacement so
           this function does nothing */
    }
  void midpoint_step_update(double delta, NLModalGeomState &stepSt);

  void printState(const char* = "");
  void printRotation(const char* = "NLModalGeomState.glR");
  void print() {}

  void setVelocity(Vector &_vel) { vel = _vel; }
  void setAcceleration(Vector &_acc) { acc = _acc; }
  void setVelocityAndAcceleration(Vector &_vel, Vector &_acc) { vel = _vel; acc = _acc; }
  void pull_back(Vector &_acc) {}
  void push_forward(Vector &_acc) {}

  friend class NLModalDescr;
};

#endif
