#include <Problems.d/NLModalGeomState.h>
#include <Corotational.d/utilities.h>
#include <Utils.d/SolverInfo.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

extern SolverInfo &solInfo;

NLModalGeomState::NLModalGeomState(int nRBM, int nFlex, int nConstr, double _cg[3]) :
  q(nFlex, 0.0), lam(nConstr, 0.0), vel(nFlex+nRBM, 0.0),
  acc(nFlex+nRBM, 0.0), numRBM(nRBM), numFlex(nFlex), numConstr(nConstr), qnp1(nFlex, 0.0){
/*PRE: NLModalGeomState instantiated with a given number of rigid body modes,
         flexible modes and constraints
 POST: parameterized constructor
*/
  glT[0] = glTnp1[0] = 0.0;
  glT[1] = glTnp1[1] = 0.0;
  glT[2] = glTnp1[2] = 0.0;

  glR[0][0] = 1.0; glR[0][1] = 0.0; glR[0][2] = 0.0;
  glR[1][0] = 0.0; glR[1][1] = 1.0; glR[1][2] = 0.0;
  glR[2][0] = 0.0; glR[2][1] = 0.0; glR[2][2] = 1.0;

  glRnp1[0][0] = 1.0; glRnp1[0][1] = 0.0; glRnp1[0][2] = 0.0;
  glRnp1[1][0] = 0.0; glRnp1[1][1] = 1.0; glRnp1[1][2] = 0.0;
  glRnp1[2][0] = 0.0; glRnp1[2][1] = 0.0; glRnp1[2][2] = 1.0;

  cg[0] = _cg[0];
  cg[1] = _cg[1];
  cg[2] = _cg[2];
}

//------------------------------------------------------------------------------

NLModalGeomState::NLModalGeomState(const NLModalGeomState& mgs) : q(mgs.q),
   lam(mgs.lam), vel(mgs.vel), acc(mgs.acc), numRBM(mgs.numRBM),
   numFlex(mgs.numFlex), numConstr(mgs.numConstr), qnp1(mgs.qnp1){
/*PRE: none
 POST: copy constructor
*/
  for(int i = 0; i < 3; ++i) {
    glT[i] = mgs.glT[i];
    glTnp1[i] = mgs.glTnp1[i];
    for(int j = 0; j < 3; ++j) {
      glR[i][j] = mgs.glR[i][j];
      glRnp1[i][j] = mgs.glRnp1[i][j];
    }
    cg[i] = mgs.cg[i];
  }
}

//------------------------------------------------------------------------------

void NLModalGeomState::update(const Vector &dsp, double dton2){
/*PRE: *this, vel and acc are approximations to the solution at n+1/2
       dsp is the difference between the next approx and the current one
 POST: update *this to be the next approximation to soln at n+1/2
*/
  glT[0] += dsp[0];  vel[0] += dsp[0]/dton2;  acc[0] += dsp[0]/(dton2*dton2);
  glT[1] += dsp[1];  vel[1] += dsp[1]/dton2;  acc[1] += dsp[1]/(dton2*dton2);
  glT[2] += dsp[2];  vel[2] += dsp[2]/dton2;  acc[2] += dsp[2]/(dton2*dton2);

  glTnp1[0] += 2*dsp[0];
  glTnp1[1] += 2*dsp[1];
  glTnp1[2] += 2*dsp[2];

  double dtheta[3] = { dsp[3], dsp[4], dsp[5] };
  inc_rottensor(glR, dtheta);

  vel[3] += dsp[3]/dton2; acc[3] += dsp[3]/(dton2*dton2);
  vel[4] += dsp[4]/dton2; acc[4] += dsp[4]/(dton2*dton2);
  vel[5] += dsp[5]/dton2; acc[5] += dsp[5]/(dton2*dton2);

  dtheta[0] = 2*dsp[3];
  dtheta[1] = 2*dsp[4];
  dtheta[2] = 2*dsp[5];
  inc_rottensor(glRnp1, dtheta);

  int b, bRBM;
  for(b = 0; b < q.size(); ++b) {
    bRBM = b+numRBM;
    q[b] += dsp[bRBM];
    qnp1[b] += 2*dsp[bRBM];
    vel[bRBM] += dsp[bRBM]/dton2;
    acc[bRBM] += dsp[bRBM]/(dton2*dton2);
  }
  for(b = 0; b < numConstr; ++b) {
    lam[b] += dsp[numRBM+q.size()+b];
  }

}

//------------------------------------------------------------------------------

void NLModalGeomState::midpoint_step_update(double delta,
  NLModalGeomState &stepState){
/*PRE: *this, vel, acc and lam are converged to the solution at time step n+1/2
       stepState contains the solution at time step n
 POST: update *this, and stepState to the solution at n+1
*/
  int i, j;
  if(!solInfo.floatingFrame_rmpt) {
    // update velocities
    for(i = 0; i < vel.size(); ++i)
      vel[i] += delta*acc[i];

    // update displacements
    glT[0] = 2.*glT[0] - stepState.glT[0];
    glT[1] = 2.*glT[1] - stepState.glT[1];
    glT[2] = 2.*glT[2] - stepState.glT[2];
    stepState.glT[0] = glT[0];
    stepState.glT[1] = glT[1];
    stepState.glT[2] = glT[2];
  }
  else {
    // update velocities
    for(i = numRBM; i < vel.size(); ++i)
      vel[i] += delta*acc[i];
  }

  double dtheta[3];
  double dRot[3][3];
  mat_mult_mat(stepState.glR, glR, dRot, 1); // dRot = stepState.glR^T * glR <--> glR = stepState.glR * dRot
  mat_to_vec(dRot, dtheta);
  inc_rottensor(glR, dtheta); // glR = glR * vec_to_mat(dtheta)

  if(solInfo.floatingFrame_rmpt) {
    double& x0 = solInfo.floatingFrame_x0;
    double& y0 = solInfo.floatingFrame_y0;
    double& z0 = solInfo.floatingFrame_z0;
    double r[3] = { cg[0] + glT[0] - x0, cg[1] + glT[1] - y0, cg[2] + glT[2] - z0 }, rnp1[3];
    mat_mult_vec(dRot, r, rnp1);
    // update displacements
    glT[0] += (rnp1[0] - r[0]);
    glT[1] += (rnp1[1] - r[1]);
    glT[2] += (rnp1[2] - r[2]);
    stepState.glT[0] = glT[0];
    stepState.glT[1] = glT[1];
    stepState.glT[2] = glT[2];
    // update velocities
    Eigen::Vector3d Omega(vel[3], vel[4], vel[5]);
    Eigen::Vector3d rnp1vec(rnp1[0], rnp1[1], rnp1[2]);
    Eigen::Vector3d vt = Omega.cross(rnp1vec); // tangential velocity at t_{n+1}
    vel[0] = vt[0];
    vel[1] = vt[1];
    vel[2] = vt[2];
    // update accelerations
    Eigen::Vector3d at = Omega.cross(vt); // tangential acceleration at t_{n+1}
    acc[0] = at[0];
    acc[1] = at[1];
    acc[2] = at[2];
  }

  for(i = 0; i < 3; ++i)
    for(j = 0; j < 3; ++j)
      stepState.glR[i][j] = glR[i][j];

  for(i = 0; i < q.size(); ++i) {
    q[i] = 2.*q[i] - stepState.q[i];
    stepState.q[i] = q[i];
  }
/*
  for(i = 0; i < numConstr; ++i) {
    lam[i] = 2*lam[i] - stepState.lam[i];
    stepState.lam[i] = lam[i];
  }
*/
}

//------------------------------------------------------------------------------

void NLModalGeomState::printState(const char* text){
/*PRE: none
 POST: print to stderr, the private data members
*/
  if(text) { fprintf(stderr, "%s\n", text); }

  fprintf(stderr, "glT and glR:\n");
  int i;
  for(i = 0; i < 3; ++i) {
    fprintf(stderr, " %16.8e", glT[i]);
    fprintf(stderr, "    %16.8e%16.8e%16.8e\n", glR[i][0], glR[i][1], glR[i][2]);
  }

  q.print("", "  q");
  lam.print("", "lam");

  fprintf(stderr, "velocity and acceleration:\n");
  for(i = 0; i < numRBM+numFlex; ++i) {
    fprintf(stderr, "  %16.8e  %16.8e\n", vel[i], acc[i]);
  }

}

//------------------------------------------------------------------------------

void NLModalGeomState::printRotation(const char* text){

  fprintf(stderr, "%s\n", text);
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      fprintf(stderr, " %16.8e", glR[i][j]);
    }
    fprintf(stderr, "\n");
  }

}

//------------------------------------------------------------------------------
